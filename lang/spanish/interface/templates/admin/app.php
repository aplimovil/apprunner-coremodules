<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <? $this->header() ?>
    <? $this->template('admin/head') ?>
</head>
<body>
	<? $this->template('admin/header') ?>
    
    <div id="page-wrapper">
		<div id="main-wrapper">
			<div id="main-content">
	            <? if($this->title): ?>
	            <h1 class="title"><?= $this->title ?></h1>
	            <? endif; ?>

	            <? $this->content() ?>
			</div>
			<div class="clearfix"></div>
		</div>
        <? $this->template('admin/sidebar') ?>

	</div>
	<div class="clearfix"></div>
	
	<? $this->template('admin/footer') ?>
</body>
</html>