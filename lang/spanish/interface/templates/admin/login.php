<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
	<? $this->header() ?>
	<? $this->template('admin/head') ?>
	<style type="text/css">
		#login ul li {
			margin-bottom: 15px;
		}

		.ui-dialog-titlebar-close {
			display: none !important;
		}
	</style>
	<script type="text/javascript">
		$(function () {
			$('#login').dialog({
				autoOpen: true,
				width: 400,
				height: 300,
				resizable: false,
				closeOnEscape: false,
				modal: true,
				buttons: {
					"Ingresar": function () {
						document.forms.frmLogin.submit();
					}
				}
			});

			<? if (!isset($_POST['username'])): ?>
				$("#username").get(0).focus();
			<? endif; ?>
		});
	</script>
</head>

<body>
	<div id="login" title="<?= $this->name ?>">
		<? if ($invalidLogin): ?>
			<div class="response-msg error ui-corner-all">
				<span>Acceso Inválido</span>
				Lo sentimos, el usuario y contraseña que introdujo no son válidos. Por favor intente de nuevo.
			</div>
		<? endif; ?>
		<form name="frmLogin" action="<?= $this->url ?>" method="post" class="forms">
			<input type="hidden" name="p" value="<?= $_REQUEST['p'] ?>" />
			<ul>
				<li>
					<label for="username" class="desc">Usuario:</label>
					<div>
						<input type="text" class="field text full" name="username" id="username"
							value="<?= $_POST['username'] ?>" />
					</div>
				</li>
				<li>
					<label for="password" class="desc">Contraseña:</label>
					<div>
						<input type="password" value="" class="field text full" name="password" id="password" />
					</div>
				</li>
			</ul>
		</form>
	</div>
</body>

</html>