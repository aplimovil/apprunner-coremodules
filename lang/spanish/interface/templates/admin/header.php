<? if($this->isInlineMode()) return; ?>

<div id="header">
	<div id="top-menu">
		<? if($USER): ?>
		<span>Bienvenido, <strong><?= $USER->name ?></strong> <em>(<?= $USER_TYPE[$USER->type] ?>)</em></span>
		<? endif; ?>
	</div>
	<div id="sitename">
		<a href="<?= $this->translatePath("admin/home") ?>"><img src="<?= $this->translateInterfacePath("images/admin/logo.gif") ?>" alt="<?= $this->name ?>" /></a>
	</div>
	<? if($LOGIN && $LOGIN->authorized()) $this->template('admin/menu') ?>
</div>