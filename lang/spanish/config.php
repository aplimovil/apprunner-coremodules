<?php
APP::settings(array(
	'LANGUAGE' => 'spanish',
	'NAME_FIELD_LABEL' => 'Nombre'
));

ErrorLogger::settings(array(
	'message' => 'Lo sentimos, ocurrió un problema técnico. Hemos recibido el reporte y lo resolveremos a la brevedad posible. Por favor intente de nuevo mas tarde.'
));

TableView::$DEFAULT_DATE_FORMAT = 'd/m/Y';

define('MENU_SYSTEM', 'Sistema');
define('MENU_SYSTEM_USERS', 'Usuarios');
define('MENU_SYSTEM_USERACTIVITY', 'Registro de Actividad');

define('LABEL_USER', 'usuario');
define('LABEL_USER_TYPE', 'Tipo de Usuario');
define('LABEL_USER_USERNAME', 'Usuario');
define('LABEL_USER_NEW_PASSWORD', 'Nueva Contraseña');
define('LABEL_USER_NEW_PASSWORD_FIELD_COMMENTS', 'Si desea cambiar la contraseña del usuario,<br />por favor introduzca la nueva contraseña aquí.');
define('LABEL_USER_CONFIRM_PASSWORD', 'Confirmar Contraseña');
define('LABEL_USER_FULL_NAME', 'Nombre Completo');
define('LABEL_USER_EMAIL', 'E-mail');
define('LABEL_USER_STATUS', 'Status');

define('LABEL_USERACTIVITY', 'actividad');
define('LABEL_USERACTIVITY_DATE', 'Fecha/Hora');
define('LABEL_USERACTIVITY_USER', 'Usuario');
define('LABEL_USERACTIVITY_TYPE', 'Acción');
define('LABEL_USERACTIVITY_TABLE', 'Tabla');
define('LABEL_USERACTIVITY_RECORD', '# Registro');
define('LABEL_USERACTIVITY_OBJECT', 'Objeto');
define('LABEL_USERACTIVITY_OLDDATA', 'Datos Anteriores');
define('LABEL_USERACTIVITY_NEWDATA', 'Datos Nuevos');

define('ERROR_PASSWORD_MISSING', 'Es necesario introducir una contraseña para crear un usuario.');
define('ERROR_DUPLICATE_USER', 'Ya hay una cuenta registrada con este nombre de usuario. Por favor elija un nombre de usuario distinto.');
define('ERROR_USER_PASSWORD_CONFIRMATION_MISMATCH', 'Lo sentimos, las contraseñas no coinciden. Por favor intente de nuevo.');

$STATUS = array(
	STATUS_ACTIVE => 'Activo',
	STATUS_INACTIVE => 'Inactivo'
);

$YES_NO = array(
	YES => 'Si',
	NO => 'No'
);

$USER_STATUS = array(
	USER_STATUS_ACTIVE => 'Activo',
	USER_STATUS_INACTIVE => 'Inactivo'
);

$USER_ACTIVITY_TYPE = array(
	USER_ACTIVITY_TYPE_INSERT => 'Alta',
	USER_ACTIVITY_TYPE_DELETE => 'Baja',
	USER_ACTIVITY_TYPE_UPDATE => 'Cambio',
	USER_ACTIVITY_TYPE_LOGIN => 'Login',
	USER_ACTIVITY_TYPE_LOGOUT => 'Logout'
);

$MONTHS = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');

APP::onConfigLoad(function(){
	global $ADMIN_MENU, $USER_TYPE;

	Lang::load('lang-spanish');

	$USER_TYPE = array(
		USER_TYPE_ADMIN => _T('Administrador'),
		USER_TYPE_SUPERVISOR => _T('Supervisor')
	);
	
	if(!$ADMIN_MENU) $ADMIN_MENU = array();

	Util::extend($ADMIN_MENU, array(
		array('path' => 'admin/home', 'label' => 'Inicio')
	));
}, true); // Runs first

APP::onPluginsLoad(function(){
	global $ADMIN_MENU;
	
	$ADMIN_MENU[] = array('path' => 'admin/logout', 'label' => 'Salir');
}); // Runs after all config is loaded