<? if($this->isInlineMode()) return; ?>

<div id="footer">
	<div>Copyright &copy; <?= date("Y") ?> <?= $this->name ?></div>
	<div style="margin-top: 10px; font-size: 10px; color: #fff; opacity: 0.7; text-align: center;">
	Request took <?= number_format($this->getElapsedTime(), 2) ?> seconds and used <?= number_format(($this->getMemoryUsage() / (1000 * 1000)), 1) ?> MB of memory.
	</div>
	<!-- CACHE FOLDER NAME: <?= Cache::getFolderName() ?> -->
</div>