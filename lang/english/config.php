<?php
APP::settings(array(
	'LANGUAGE' => 'english',
	'NAME_FIELD_LABEL' => 'Name'
));

ErrorLogger::settings(array(
	'message' => 'We\'re sorry, a technical issue ocurred. We\'ve received the report and will fix the problem as soon as possible. Please try again later.'
));

TableView::$DEFAULT_DATE_FORMAT = 'm/d/Y';

define('MENU_SYSTEM', 'System');
define('MENU_SYSTEM_USERS', 'Users');
define('MENU_SYSTEM_USERACTIVITY', 'Activity Log');

define('LABEL_USER', 'user');
define('LABEL_USER_TYPE', 'User Type');
define('LABEL_USER_USERNAME', 'Username');
define('LABEL_USER_NEW_PASSWORD', 'New Password');
define('LABEL_USER_NEW_PASSWORD_FIELD_COMMENTS', 'If you want to change the user\'s password,<br />please enter the new password here.');
define('LABEL_USER_CONFIRM_PASSWORD', 'Confirm Password');
define('LABEL_USER_FULL_NAME', 'Full Name');
define('LABEL_USER_EMAIL', 'E-mail');
define('LABEL_USER_STATUS', 'Status');

define('LABEL_USERACTIVITY', 'activity');
define('LABEL_USERACTIVITY_DATE', 'Date/Time');
define('LABEL_USERACTIVITY_USER', 'User');
define('LABEL_USERACTIVITY_TYPE', 'Action');
define('LABEL_USERACTIVITY_TABLE', 'Table');
define('LABEL_USERACTIVITY_RECORD', 'Record #');
define('LABEL_USERACTIVITY_OBJECT', 'Object');
define('LABEL_USERACTIVITY_OLDDATA', 'Old Data');
define('LABEL_USERACTIVITY_NEWDATA', 'New Data');

define('ERROR_PASSWORD_MISSING', 'A password needs to be provided in order to create a user.');
define('ERROR_DUPLICATE_USER', 'There\'s already a user account with this username. Please choose a different username.');
define('ERROR_USER_PASSWORD_CONFIRMATION_MISMATCH', 'Sorry, the passwords don\'t match. Please try again.');

$STATUS = array(
	STATUS_ACTIVE => 'Active',
	STATUS_INACTIVE => 'Inactive'
);

$YES_NO = array(
	YES => 'Yes',
	NO => 'No'
);

$USER_STATUS = array(
	USER_STATUS_ACTIVE => 'Active',
	USER_STATUS_INACTIVE => 'Inactive'
);

$USER_ACTIVITY_TYPE = array(
	USER_ACTIVITY_TYPE_INSERT => 'Create',
	USER_ACTIVITY_TYPE_DELETE => 'Delete',
	USER_ACTIVITY_TYPE_UPDATE => 'Update',
	USER_ACTIVITY_TYPE_LOGIN => 'Login',
	USER_ACTIVITY_TYPE_LOGOUT => 'Logout'
);

$MONTHS = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 =>'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');

APP::onConfigLoad(function(){
	global $ADMIN_MENU, $USER_TYPE;

	Lang::load('lang-english');

	$USER_TYPE = array(
		USER_TYPE_ADMIN => _T('Administrator'),
		USER_TYPE_SUPERVISOR => _T('Supervisor')
	);

	if(!$ADMIN_MENU) $ADMIN_MENU = array();

	Util::extend($ADMIN_MENU, array(
		array('path' => 'admin/home', 'label' => 'Home')
	));
}, true); // Runs first

APP::onPluginsLoad(function(){
	global $ADMIN_MENU;
	
	$ADMIN_MENU[] = array('path' => 'admin/logout', 'label' => 'Logout');
}); // Runs after all config is loaded