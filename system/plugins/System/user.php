<?php
namespace System;

use API;
use Exception;

class user extends \Model {
	use \Entity, \SoftDeletes, \ValidationWithAudit;

	static $route = 'admin/system/users';
	static $order = 'name ASC';
	
	public 		$id;
	public 		$username;
	protected 	$hash;
	protected 	$salt;
	public 		$type;
	public 		$name;
	public 		$email;
	public 		$status;

	/*
	static $api = array(
		'public' => false, // Set to false to require "key" query parameter to be equal to API_KEY config constant
		'fields' => ['id', 'name'], // Specify which fields to include only in the response, set to an empty array to include all fields
		'format' => 'json', // Set to 'json' or 'xml',
		'filter' => [], // Specify a default filter as an array or string of SQL "WHERE" clauses
		'order' => 'id DESC', // Specify the default ordering for items in the response
		'limit' => 0, // Set to non-zero to specify a maximum number of items to return
		'gzip' => ['level' => 3], // Set to true or to a configuration object to enable GZIP compression of response
		'cache' => ['expiration' => 1, 'cachePOST' => false], // Set to true or to a configuration object to enable caching of response
		'restrictedParams' => [], // The list of GET/POST parameters that are not allowed to be used (can be model field names or predefined model API request params)
		'disallowUserFilter' => false, // Set to true to disallow the use of the "filter" GET/POST param
		'debug' => false // Set to true or 1 to include a "debug" property in all responses with information helpful for debugging model API requests
	);
	*/

	function name(){
		return $this->name;
	}

	function setHash($password){
		$salt = \Util::RandomString(20, true);
		$this->hash = sha1($password . $salt);
		$this->salt = $salt;
		return $this;
	}

	static function login($username, $password){
		if(ltrim($username) == '') return false;
		
		$user = static::findOne(['username' => $username]);
			
		if($user && $user->validate($password)){
			return $user;
		}
		else return false;
	}

	function validate($password){
		if($this->hash === sha1($password . $this->salt) && $this->status == STATUS_ACTIVE){
			return true;
		}
		else{
			return false;
		}
	}

	function getSignature($key){
		return hash_hmac('sha1', ($this->id . '|' . $this->hash . '|' . $this->salt), $key);
	}

	static function validateAPIAccess($userId, $key, $signature){
		$api = new API;
		
		try{
			if($user = static::get($userId)){
				if($signature == $user->getSignature($key)){
					if($user->status == USER_STATUS_ACTIVE){
						return $user;
					}
					else throw new Exception(_T("The user account is inactive."));
				}
				else throw new Exception(_T("The signature isn't valid."));
			}
			else throw new Exception(_T("The user account doesn't exist."));
		}
		catch(Exception $e){
			$api->error($e->getMessage());
		}
	}
}