<?php
class user_activity extends Model {
	static function record($aData){
		global $USER;
		
		if(!isset($aData['user_id'])) $aData['user_id'] = $USER->id;
		if(!isset($aData['date'])) $aData['date'] = date("Y-m-d H:i:s");
		
		$record = new user_activity();
		$record->populate($aData);
		return $record->save();
	}
}