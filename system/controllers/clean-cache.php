<?php
class CoreSystem_CleanCacheController extends Controller {
	static $deleteAfterDays = 1;

	function main($APP, $enableOutput = true){
		$this->disableView();

		$apprunnerCachePath = Cache::getFolderPath();

		if($apprunnerCachePath != ''){
			$this->cleanCache($apprunnerCachePath, $enableOutput);
		}
		elseif($enableOutput) echo "ERROR: AppRunner cache folder path is not available.<br />";

		$this->cleanCache($APP->translateDataPath('tmp/', true), $enableOutput);
	}

	function cleanCache($path, $enableOutput = true){
		if($enableOutput) echo "<p>";

		if(is_dir($path)){
			$command = "find \"{$path}\" -type f -mtime +" . self::$deleteAfterDays . ' -delete';
			

			if($enableOutput){
				echo "Cleaning cache folder ({$path})...<br />";
			}
			
			$result = Util::exec($command);

			if(!$result->error){
				if($enableOutput){
					echo "Cache path cleaned successfully...<br />";
					echo "<pre>{$result->output}</pre>";
					echo 'Done.';
				}
			}
			else{
				if($enableOutput) echo "ERROR:<br /><pre>{$result->error}</pre>";
			}
		}
		elseif($enableOutput) echo "NOTICE: Cache folder doesn't exist yet ({$path}).<br />";

		if($enableOutput) echo "</p>";
	}
}