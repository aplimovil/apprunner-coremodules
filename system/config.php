<?php
define('STATUS_DELETED', 0);
define('STATUS_ACTIVE', 1);
define('STATUS_INACTIVE', '2');

define('YES', 1);
define('NO', 2);

define('ON', 1);
define('OFF', 0);

define('USER_TYPE_ADMIN', 1);
define('USER_TYPE_SUPERVISOR', 2);

define('USER_STATUS_ACTIVE', 1);
define('USER_STATUS_INACTIVE', 2);

define('USER_ACTIVITY_TYPE_INSERT', 1);
define('USER_ACTIVITY_TYPE_DELETE', 2);
define('USER_ACTIVITY_TYPE_UPDATE', 3);
define('USER_ACTIVITY_TYPE_LOGIN', 4);
define('USER_ACTIVITY_TYPE_LOGOUT', 5);

/* Run the cache cleaner in this % of requests (approximate). Increase to auto-clean cache more often, decrease it otherwise.
To disable auto-cleaning and do manual cleaning, set to 0 and set up a cron task to call the /clean-cache route at the desired interval. */
$AUTO_CLEAN_CACHE_PROBABILITY = 3;

APP::onViewLoad(function($APP) use($AUTO_CLEAN_CACHE_PROBABILITY){
	if($AUTO_CLEAN_CACHE_PROBABILITY){
		if(mt_rand(0, 100) <= $AUTO_CLEAN_CACHE_PROBABILITY){
			$APP->loadModuleController('core/system', 'clean-cache');
			$cacheCleaner = new CoreSystem_CleanCacheController;
			$cacheCleaner->main($APP, false);
		}
	}
});