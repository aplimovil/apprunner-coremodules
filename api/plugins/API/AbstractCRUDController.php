<?php
namespace API;

abstract class AbstractCRUDController extends \APIController {
	use CRUD;

	function init($action, $GET, $POST){
		global $APP;

		$this->modelName = $GET->_model;
		$this->action = $action;

		$this->prepare();
		$this->loadParams($POST);
		$this->verifyOwner();
		$this->validateOwnership();
		$this->rateLimit();
	}
}