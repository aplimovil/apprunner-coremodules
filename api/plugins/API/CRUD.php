<?php
namespace API;

/*
Sample configuration settings for "api" static property in model:

	static $api = [
		'get' => [bool], // default: true (unless read=true or crud=true) / whether to allow the non-authenticated GET API on the model
		'create' => [bool], // default: true (unless crud=false) / whether to allow the CREATE API on the model
		'read' => [bool], // default: true (unless crud=false) / whether to allow the READ API on the model
		'update' => [bool], // default: true (unless crud=false) / whether to allow the UPDATE API on the model
		'delete' => [bool], // default: true (unless crud=false) / whether to allow the DELETE API on the model
		'crud' => [bool], // default: false / whether to allow all API CRUD operations on the model
		'fields' => [array], // Only return this fields from the model
		'lockedFields' => [array], // Do not allow updating these fields (model's primary key is included by default)
		'validateOwnership' => [bool], // default: true
		'onlyOwnerReads' => [bool], // default: true / whether to only allow the owner of a record to read it if validateOwnership=true
		'anonymousReads' => [bool], // default: false / whether to allow reads without providing OWNER credentials
		'verifyPassword' => [bool], // default: true (unless dynamicOwner=true) / verify CREATE, UPDATE, and DELETE operations by requesting the password of the owner
		'secureReads' => [bool], // default: false / whether to also secure READ operations by requiring password verification
		'passwordHashField' => [string], // default: "password_hash" / field that contains the secure hash of the owner's password
		'selfOwner' => [bool], // default: false / whether to use the model's ID and token field as the owner of itself (use when creating a user account and needing the account ID and token to allow updating it later on)
		'dynamicOwner' => [bool], // default: false / whether to automatically generate an owner ID (the client's IP address or an auto-generated UUID if useIPAddress=false) and a token (a long random string) for create operations if the _OWNER_ID and _OWNER_TOKEN parameters are not passed, which are returned in the create endpoint response
		'useIPAddress' => [bool], // default: true / used by dynamicOwner, whether to use the hex-encoded IP address as a dynamic owner ID instead of an auto-generated UUID
		'ownerModel' => [string], // model name of owner (required only if validateOwnership=true, set to the class itself if selfOwner=true)
		'ownerTokenField' => [string], // default: token / name of field in owner model that holds the "token" value for authentication
		'ownerForeignKey' => [string or bool], // default: concatenation of ownerModel, an underscore, and the primary key name of the model (e.g. model_id) / set to "false" to prevent filtering of entities by owner ID in "read" operations
		'waitTime' => [number], // default: 1 / number of seconds before the owner is able to update/delete another entity
		'createWaitTime' => [number] // default: 5 / number of seconds before the owner is able to create another entity, overriden by waitTime if it's larger than createWaitTime
		'waitMessage' => [string],
		'debug' => [bool], // default: false
		'useTextStatus' => [bool],
		'errorField' => [string],
		'public' => [bool] // default: false
	];

NOTES:
- The endpoints are available under api/[operation]/[model] (e.g. api/create/model_name, api/read/model_name, etc.)
- The create/update/delete endpoints receive the POST/GET parameters "_OWNER_ID", "_OWNER_TOKEN", "_OWNER_PASSWORD" (unless verifyPassword=false), and "_ENTITY_ID" (except for a "create" operation) in order to authenticate the operation
- The above parameters are sent via GET for read operations and via POST for create/update/delete
*/

use DB, Util, Exception, Model;

trait CRUD {
	protected $modelName;
	protected $action;
	protected $entityId;
	protected $ownerId;
	protected $ownerToken;
	protected $ownerPassword;
	protected $owner;

	protected $fields;
	protected $lockedFields = array();
	protected $selfOwner = false;
	protected $dynamicOwner = false;
	protected $useIPAddress = true;
	protected $ownerModel;
	protected $ownerTokenField = 'token';
	protected $ownerForeignKey;
	protected $validateOwnership = true;
	protected $verifyPassword = true;
	protected $secureReads = false;
	protected $onlyOwnerReads = true;
	protected $anonymousReads = false;
	protected $passwordHashField = 'password_hash';
	protected $debug = false;
	protected $waitTime = 1;
	protected $createWaitTime = 5;
	protected $waitMessage = 'Please wait before doing this operation again.';

	function prepare(){
		global $APP;

		if($APP->modelExists($this->modelName)){
			$modelClassName = '\\' . $this->modelName;

			if(property_exists($modelClassName, 'api')){
				if($modelClassName::$api === false){
					$this->invalidRequest();
				}

				if($modelClassName::$api[ACTION_READ] === true){
					$modelClassName::$api[ACTION_GET] = false;
				}

				if($modelClassName::$api['crud'] === true){
					$modelClassName::$api[ACTION_GET] = false;
					$modelClassName::$api[ACTION_CREATE] = true;
					$modelClassName::$api[ACTION_READ] = true;
					$modelClassName::$api[ACTION_UPDATE] = true;
					$modelClassName::$api[ACTION_DELETE] = true;
				}

				if(!isset($modelClassName::$api[$this->action]) || $modelClassName::$api[$this->action] !== true){
					$this->error(_T('%s operation not allowed on model', strtoupper($this->action)));
				}

				if(isset($modelClassName::$api['debug'])){
					$this->debug = $modelClassName::$api['debug'];
					if($this->debug) $this->includeDebugInfo();
				}

				if($modelClassName::$api['useTextStatus']){
					$this->useTextStatus();
				}

				if(isset($modelClassName::$api['errorField'])){
					$this->setErrorField($modelClassName::$api['errorField']);
				}

				if(!$modelClassName::$api['public']){
					$this->authorize();
				}

				if(isset($modelClassName::$api['fields']) && is_array($modelClassName::$api['fields'])){
					$this->fields = $modelClassName::$api['fields'];
				}

				if(isset($modelClassName::$api['lockedFields']) && is_array($modelClassName::$api['lockedFields'])){
					$this->lockedFields = $modelClassName::$api['lockedFields'];
					if(!is_array($this->lockedFields)) throw new Exception(_T('$api[lockedFields] must be an array'));
				}

				$modelName = $this->getModelName();
				array_unshift($this->lockedFields, $modelClassName::getPrimaryKeyName());

				if(isset($modelClassName::$api['selfOwner'])){
					$this->selfOwner = $modelClassName::$api['selfOwner'];
				}
				
				if(isset($modelClassName::$api['dynamicOwner'])){
					$this->dynamicOwner = $modelClassName::$api['dynamicOwner'];
				}

				if(isset($modelClassName::$api['useIPAddress'])){
					$this->useIPAddress = $modelClassName::$api['useIPAddress'];
				}

				if(isset($modelClassName::$api['validateOwnership'])){
					$this->validateOwnership = $modelClassName::$api['validateOwnership'];
				}

				if(isset($modelClassName::$api['verifyPassword'])){
					$this->verifyPassword = $modelClassName::$api['verifyPassword'];
				}

				if(isset($modelClassName::$api['secureReads'])){
					$this->secureReads = $modelClassName::$api['secureReads'];
				}

				if(isset($modelClassName::$api['onlyOwnerReads'])){
					$this->onlyOwnerReads = $modelClassName::$api['onlyOwnerReads'];
				}

				if(isset($modelClassName::$api['anonymousReads'])){
					$this->anonymousReads = $modelClassName::$api['anonymousReads'];
				}

				if(isset($modelClassName::$api['passwordHashField'])){
					$this->passwordHashField = $modelClassName::$api['passwordHashField'];
				}

				if(isset($modelClassName::$api['ownerModel'])){
					$this->ownerModel = $modelClassName::$api['ownerModel'];
				}

				if(isset($modelClassName::$api['ownerTokenField'])){
					$this->ownerTokenField = $modelClassName::$api['ownerTokenField'];
				}

				if(isset($modelClassName::$api['ownerForeignKey'])){
					$this->ownerForeignKey = $modelClassName::$api['ownerForeignKey'];

					if(!is_bool($ownerForeignKey) && !is_string($ownerForeignKey)){
						throw new Exception(_T('Invalid value for $api[ownerForeignKey] setting'));
					}
				}
				else{
					if($this->selfOwner){
						$this->ownerModel = $modelName;
					}

					if($ownerModel = $this->ownerModel){
						$this->ownerForeignKey = $ownerModel . '_' . $ownerModel::getPrimaryKeyName();
					}
				}

				if(isset($modelClassName::$api['waitTime'])){
					$this->waitTime = $modelClassName::$api['waitTime'];
				}

				if(isset($modelClassName::$api['createWaitTime'])){
					$this->createWaitTime = $modelClassName::$api['createWaitTime'];
				}

				if($this->waitTime > $this->createWaitTime){
					$this->createWaitTime = $this->waitTime;
				}

				if(isset($modelClassName::$api['waitMessage'])){
					$this->waitMessage = $modelClassName::$api['waitMessage'];
				}

				if($this->dynamicOwner){
					$this->verifyPassword = false;
				}
			}
			else $this->invalidRequest();
		}
		else $this->invalidRequest();
	}

	function loadParams($params){
		$this->ownerId = trim($params->_OWNER_ID);
		$this->ownerToken = trim($params->_OWNER_TOKEN);
		$this->ownerPassword = trim($params->_OWNER_PASSWORD);
		$this->entityId = trim($params->_ENTITY_ID);

		if($this->action == ACTION_CREATE){
			if($this->dynamicOwner){
				if(!$this->ownerId){
					if($this->ownerToken) $this->error(_T('Owner token not provided'));

					$this->ownerId = $this->useIPAddress ? sha1(Util::getRealIPAddress()) : Util::generateUUID();
					$this->ownerToken = Util::generateToken();
				}
			}
			elseif($this->selfOwner){
				$this->ownerId = 'SELF';
				$this->ownerToken = 'SELF';
			}
		}

		if($this->areOwnerCredentialsRequired()){
			if(!$this->ownerId) $this->error(_T('Owner ID not provided'));
			if(!$this->ownerToken) $this->error(_T('Owner token not provided'));
		}
		
		if(!$this->entityId && in_array($this->action, array(ACTION_UPDATE, ACTION_DELETE))){
			$this->error(_T('Entity ID not provided'));
		}
	}

	protected function areOwnerCredentialsRequired(){
		return ($this->action != ACTION_READ || !$this->anonymousReads);
	}

	function verifyOwner(){
		global $APP;

		if(!$this->areOwnerCredentialsRequired()) return;

		if($this->ownerModel){
			if($this->action != ACTION_CREATE || !$this->selfOwner){
				$modelName = '\\' . $this->modelName;
				$ownerModel = '\\' . $this->ownerModel;
				$tokenField = $this->ownerTokenField;
				$passwordHashField = $this->passwordHashField;

				$this->owner = $ownerModel::get($this->getOwnerId());

				if($this->owner){
					if($this->owner->{$tokenField} != $this->getOwnerToken()){
						$this->error(_T('Owner token is invalid'), ['code' => 'invalidToken']);
					}

					if($this->verifyPassword){
						if($this->action != ACTION_READ || $this->secureReads){
							if(!$this->ownerPassword){
								$this->error(_T('No password was provided.'));
							}
							elseif(!Util::verifyPassword($this->ownerPassword, $this->owner->{$passwordHashField})){
								$this->error(_T('Sorry, the password is incorrect.'), ['code' => 'passwordIncorrect']);
							}
						}
					}
				}
				else $this->error(_T('Owner not found'));
			}
		}
		elseif($this->action != ACTION_CREATE){
			if($this->action == ACTION_READ && !$this->secureReads) return;

			$token = DB::GetValue('SELECT owner_token FROM _api_entity WHERE owner_id = ? AND model = ? LIMIT 1', [$this->getOwnerId(), $this->getModelName()]);

			if($token){
				if($token != $this->getOwnerToken()){
					$this->error(_T('Owner token is invalid'), ['code' => 'invalidToken']);
				}
			}
			else throw new Exception(_T('$api[ownerModel] is missing from the model configuration and no previous ownership tokens were found for reference'));
		}
	}

	function validateOwnership(){
		if(!$this->validateOwnership) return;

		if($this->action != ACTION_CREATE){
			if($token = DB::GetValue('SELECT owner_token FROM _api_entity WHERE model = ? AND entity_id = ? AND owner_id = ?', [$this->getModelName(), $this->getEntityId(), $this->getOwnerId()])){
				if($token != $this->getOwnerToken()){
					$this->error(_T('Entity owner token does not match'), ['code' => 'invalidToken']);
				}
			}
			else $this->error(_T('Entity ownership not found'), ['code' => 'invalidToken']);
		}
	}

	function rateLimit(){
		$waitTime = $this->action == ACTION_CREATE ? $this->createWaitTime : $this->waitTime;

		switch($this->action){
			case ACTION_CREATE:
				$timeField = 'created_time';
				break;
			case ACTION_UPDATE:
				$timeField = 'updated_time';
				break;
			case ACTION_DELETE:
				$timeField = 'deleted_time';
				break;
			default:
				return;
		}

		$lastCreatedTime = DB::GetValue("SELECT {$timeField} FROM _api_entity WHERE owner_id = ? ORDER BY {$timeField} DESC LIMIT 1", [$this->ownerId]);

		if($lastCreatedTime && (time() - $lastCreatedTime) < $waitTime){
			$this->error(_T($this->waitMessage));
		}
	}

	function createOwnership(Model $entity){
		$this->entityId = $entity->getId();

		if($this->selfOwner){
			$this->ownerId = $entity->getId();
			$this->ownerToken = $entity->{$this->ownerTokenField};
		}
		
		DB::Query('INSERT INTO _api_entity (model, entity_id, owner_id, owner_token, created_time) VALUES(?, ?, ?, ?, ?)', [$this->getModelName(), $this->getEntityId(), $this->getOwnerId(), $this->getOwnerToken(), time()]);
	}

	function registerUpdate(){
		DB::Query('UPDATE _api_entity SET updated_time = ? WHERE model = ? AND entity_id = ? AND owner_id = ?', [time(), $this->getModelName(), $this->getEntityId(), $this->getOwnerId()]);
	}

	function registerDelete(){
		DB::Query('UPDATE _api_entity SET deleted_time = ? WHERE model = ? AND entity_id = ? AND owner_id = ?', [time(), $this->getModelName(), $this->getEntityId(), $this->getOwnerId()]);
	}

	function getModelName(){
		return $this->modelName;
	}

	function getAction(){
		return $this->action;
	}

	function getFields(){
		return $this->fields;
	}

	function getLockedFields(){
		return $this->lockedFields;
	}

	function isDynamicOwner(){
		return $this->dynamicOwner;
	}

	function getOwnerId(){
		return $this->ownerId;
	}

	function getOwnerToken(){
		return $this->ownerToken;
	}

	function getOwnerModel(){
		return $this->ownerModel;
	}

	function getOwnerForeignKey(){
		return $this->ownerForeignKey;
	}

	function getOwner(){
		return $this->owner;
	}

	function getEntityId(){
		return $this->entityId;
	}
}