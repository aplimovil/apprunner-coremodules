<?php
namespace API;

/*
Sample configuration settings for "api" static property in model:

	static $api = [
		'get' => [bool], // default: true / whether to allow the "get" API on the model
		'fields' => [array], // Only return this fields from the model
		'restrictedParams' => [array],
		'disallowUserFilter' => [bool],
		'filter' => [array or string],
		'order' => [string],
		'limit' => [int],
		'format' => [string: "json" or "xml"],
		'uploadFields' => [array],
		'multiUploadFields' => [array],
		'imageUploadFields' => [array],
		'imageMultiUploadFields' => [array],
		'parents' => [foreign_key => model_name, ...] or [foreign_key => [model_name, fields, newFieldName]]
		'debug' => [bool], // default: false
		'gzip' => [bool or array config], // default: false
		'cache' => [bool] or [expiration] or ['expiration' => X] where "expiration" or "X" is the cache expiration in minutes (5 minutes by default)
		'useTextStatus' => [bool],
		'errorField' => [string],
		'public' => [bool] // default: false
	];

NOTES:
- The endpoint is available under api/get/[model] (e.g. api/get/model_name)
*/

use DB, Util, Exception;
use stdClass;

abstract class AbstractRequestController extends \APIController {
	static $defaultCacheExpiration = 5; // in minutes
			
	function run($params, $isCRUD = false, $onlyOwnerReads = false){
		global $APP;

		$modelName = $params->_model;

		if($APP->modelExists($modelName)){
			$modelName = '\\' . $modelName;
			$modelInstance = new $modelName;

			$fieldsToReturn = array();
			$debug = false;
			$outputFormat = 'json';
			$defaultOrder = false;
			$defaultLimit = false;
			$where = array();
			$restrictedParams = null;
			$disallowUserFilter = false;
			$uploadFields = null;
			$multiUploadFields = null;
			$imageUploadFields = null;
			$imageMultiUploadFields = null;
			$parents = null;

			// Apply API configuration specified in model
			if(property_exists($modelName, 'api')){
				// If model has an API preprocessor filter
				if(method_exists($modelInstance, 'API_Before')){
					$this->_verifyAPIFiltersInterface($modelInstance);

					/*
					NOTE: The API_Before() static method in the model class definition should have the $params parameter passed by reference (& operator)
					-----
					class modelName extends Model {
						public static function API_Before(object &$params){
							// Modify $params object here
						}
					}
					-----
					*/
					
					// Run preprocessor filter
					$modelInstance::API_Before($params);
				}

				if($modelName::$api === false){
					$this->invalidRequest();
				}

				if($modelName::$api['get'] === false && !$isCRUD){
					$this->error(_T('GET API not enabled, use $api[get=true] to enable'));
				}

				if(isset($modelName::$api['fields']) && is_array($modelName::$api['fields'])){
					$fieldsToReturn = $modelName::$api['fields'];
				}

				if(isset($modelName::$api['restrictedParams'])){
					$restrictedParams = $modelName::$api['restrictedParams'];
				}

				if(isset($modelName::$api['disallowUserFilter'])){
					$disallowUserFilter = $modelName::$api['disallowUserFilter'];
				}

				if(isset($modelName::$api['filter'])){
					$filter = $modelName::$api['filter'];

					if(is_array($filter)){
						if(count($filter) != 0){
							$where[] = '(' . UTIL::arrayToSQLFilter($filter) . ')';
						}
					}
					elseif(is_string($filter)){
						if(ltrim($filter) != ''){
							$where[] = '(' . $filter . ')';
						}
					}
					else throw new Exception(_T('APIRequestController: The api.filter attribute in the "%s" model is not an array nor a string with a SQL filter.', $modelName));
				}

				if(isset($modelName::$api['order']) && is_string($modelName::$api['order'])){
					$defaultOrder = $modelName::$api['order'];
				}
				elseif(property_exists($modelName, 'order')){
					$defaultOrder = $modelName::$order;
				}

				if(isset($modelName::$api['limit'])){
					$defaultLimit = $modelName::$api['limit'];
				}

				if(isset($modelName::$api['format'])){
					$outputFormat = $modelName::$api['format'];
				}

				if($modelName::$api['uploadFields']){
					if(!Util::classHasTrait($modelName, 'Uploads')) throw new Exception(_T('The model "%s" requires the Uploads trait to use the api.uploadFields setting.'));
					$uploadFields = $modelName::$api['uploadFields'];
				}

				if($modelName::$api['multiUploadFields']){
					if(!Util::classHasTrait($modelName, 'Uploads')) throw new Exception(_T('The model "%s" requires the Uploads trait to use the api.multiUploadFields setting.'));
					$multiUploadFields = $modelName::$api['multiUploadFields'];
				}

				if($modelName::$api['imageUploadFields']){
					if(!Util::classHasTrait($modelName, 'Uploads')) throw new Exception(_T('The model "%s" requires the Uploads trait to use the api.imageUploadFields setting.'));
					if(!Util::classHasTrait($modelName, 'ImageResizer')) throw new Exception(_T('The model "%s" requires the ImageResizer trait to use the api.imageUploadFields setting.'));
					$imageUploadFields = $modelName::$api['imageUploadFields'];
				}

				if($modelName::$api['imageMultiUploadFields']){
					if(!Util::classHasTrait($modelName, 'Uploads')) throw new Exception(_T('The model "%s" requires the Uploads trait to use the api.imageMultiUploadFields setting.'));
					if(!Util::classHasTrait($modelName, 'ImageResizer')) throw new Exception(_T('The model "%s" requires the ImageResizer trait to use the api.imageMultiUploadFields setting.'));
					$imageMultiUploadFields = $modelName::$api['imageMultiUploadFields'];
				}

				if($modelName::$api['parents']){
					$parents = $modelName::$api['parents'];
				}

				if(isset($modelName::$api['debug'])){
					if($modelName::$api['debug']){
						$debug = true;
						$this->includeDebugInfo();
					}
				}

				if(isset($modelName::$api['log'])){
					if($modelName::$api['log']) \APIController::$enableLog = true;
				}

				if(isset($modelName::$api['gzip']) && $modelName::$api['gzip'] !== false){ // Enable GZIP if the gzip option is not set to false
					$compressionLevel = null;

					if($modelName::$api['gzip']['level']){
						$compressionLevel = $modelName::$api['gzip']['level'];
					}

					$this->enableGZip($compressionLevel);
				}

				if(isset($modelName::$api['cache']) && $modelName::$api['cache'] !== false && $params->geosearch != '1'){ // Cache requests if the cache option is not set to false and if the request is not for a geospatial search
					$cacheExpiration = static::$defaultCacheExpiration;
					
					if(is_int($modelName::$api['cache'])){
						$cacheExpiration = $modelName::$api['cache'];
					}
					elseif($modelName::$api['cache']['expiration']){
						$cacheExpiration = $modelName::$api['cache']['expiration'];
					}

					$this->enableCache($cacheExpiration);
				}

				if($modelName::$api['useTextStatus']){
					$this->useTextStatus();
				}

				if(isset($modelName::$api['errorField'])){
					$this->setErrorField($modelName::$api['errorField']);
				}

				if(!$modelName::$api['public']){
					$this->authorize();
				}
			}
			else{
				$this->invalidRequest();
			}

			$this->assertSafeParameter($params->order);
			$this->assertSafeParameter($params->limit);
			$this->assertSafeParameter($params->offset);
			$this->assertSafeParameter($params->page);
			$this->assertSafeParameter($params->filter);
			$this->assertSafeParameter($params->where); // DEPRECATED

			// Load table information
			$tableName = DB::Class2Table($modelName);
			$tableColumns = DB::Columns($modelName);
			$modelProperties = Util::getPublicProperties($modelName);

			$columns = array('*');
			$from = DB::EscapeField($tableName);
			$having = array();
			$groupBy = '';
			$order = isset($params->order) ? trim($params->order) : $defaultOrder;
			$limit = isset($params->limit) ? trim($params->limit) : $defaultLimit;
			$offset = isset($params->offset) && is_numeric($params->offset) ? $params->offset : 0;
			if(isset($params->page) && is_numeric($params->page)) $offset = ($params->page - 1) * $limit;

			if(isset($params->format)){
				$outputFormat = $params->format;
			}

			if(isset($params->where)){ // DEPRECATED
				$params->filter = $params->where;
			}

			// Parse filter parameter
			if(isset($params->filter)){
				if($disallowUserFilter !== true){
					$filter = trim($params->filter);

					if($filter != ''){
						if($APP->settings->SANITIZE_PARAMS_ADD_SLASHES){
							$filter = stripslashes($filter);
						}
						
						// Check if filter doesn't have any malicious keywords
						if(Util::isSafeSQLFilter($filter)){
							$where[] = '(' . $filter . ')';
						}
						else{
							$this->error(_T('The filter parameter cannot contain any SQL clauses.'));
						}
					}
				}
				else{
					$this->error(_T('User filtering is not allowed in this API endpoint.'));
				}
			}

			// Check parameters for model fields
			foreach($params as $key => $value){
				if($restrictedParams){
					if(in_array($key, $restrictedParams)){
						$this->error(_T("The '%s' parameter is restricted for this API endpoint.", $key));
					}
				}

				if(array_key_exists($key, $tableColumns)){ // If parameter name is a column in the model's table
					if(!in_array($key, array('latitude', 'longitude'))){ // If it is not one of the reserved parameters
						$where[] = DB::EscapeField("{$tableName}.{$key}") . " = " . DB::EscapeValue($value); // Add the filter
					}
				}
			}

			// Create keyword search filter if "keywords" parameter is passed
			if($params->keywords){
				$keywords = $params->keywords;
				$matchFields = isset($params->match) ? explode(',', str_replace(' ', '', $params->match)) : array($APP->settings->NAME_FIELD);

				$condition = array();

				foreach($matchFields as $field){
					$condition[] = DB::EscapeField($field) . " LIKE " . DB::EscapeValue("%{$keywords}%");
				}

				$condition = '(' . join(' OR ', $condition) . ')';

				$where[] = $condition;
			}

			// Create split keyword search filter if "splitwords" parameter is passed, which only returns records that contain all the individual words of the split keywords passed (separated by spaces, commas, new lines or tabs), without regard to the order in which they appear in the target field
			if($params->splitwords){
				$splitWords = preg_split("/[ ,\n\t]/", $params->splitwords);
				$matchFields = isset($params->match) ? explode(',', str_replace(' ', '', $params->match)) : array($APP->settings->NAME_FIELD);

				$condition = array();

				foreach($matchFields as $field){
					$subcondition = array();
					foreach($splitWords as $word){
						$subcondition[] = DB::EscapeField($field) . " LIKE " . DB::EscapeValue("%{$word}%");
					}
					$condition[] = '(' . join(' AND ', $subcondition) . ')';
				}

				$condition = '(' . join(' OR ', $condition) . ')';

				$where[] = $condition;
			}

			// Create find-in-set filter if "contains" parameter is passed
			if($params->contains){
				if($params->match){
					$where[] = 'FIND_IN_SET(' . DB::EscapeValue($params->contains) . ', ' . DB::EscapeField($params->match) . ')';
				}
				else $this->error(_T('The "match" parameter is required.'));
			}

			// Create geospatial search filter if "geosearch", "latitude" and "longitude" parameters are passed
			if($params->geosearch == '1' && is_numeric($params->latitude) && is_numeric($params->longitude)){
				$geolocationFieldName = property_exists($modelName, 'geosearchLocationField') ? $modelInstance::$geosearchLocationField : 'geolocation';
				$latitudeFieldName = property_exists($modelName, 'geosearchLatitudeField') ? $modelInstance::$geosearchLatitudeField : 'latitude';
				$longitudeFieldName = property_exists($modelName, 'geosearchLongitudeField') ? $modelInstance::$geosearchLongitudeField : 'longitude';

				if(!isset($params->radius) || !is_numeric($params->radius)){
					$this->error(_T('The "radius" parameter is required and should be a numeric value in kilometers.'));
				}

				$radius = floatval($params->radius);

				$columns[] = "(glength(linestringfromwkb(linestring({$geolocationFieldName}, PointFromText('POINT({$params->latitude} {$params->longitude})')))) * 111.16581) AS distance";
				
				$box = Util::getGeoBoundingBox($params->latitude, $params->longitude, $radius);
				$where[] = sprintf("({$latitudeFieldName} BETWEEN %s and %s AND {$longitudeFieldName} BETWEEN %s and %s)", $box->SW->latitude, $box->NW->latitude, $box->NW->longitude, $box->NE->longitude);

				$having[] = "distance <= {$radius}";

				if(!$params->order){
					$order = 'distance ASC';
				}

				$this->disableCache();
			}

			// Create "newer than timestamp" filter if timestamp parameter is passed
			if(isset($params->timestamp) && is_numeric($params->timestamp)){
				$where[] = 'timestamp > ' . DB::Escape($params->timestamp);
			}

			// Apply status filter if model uses SoftDeletes trait
			if(Util::classHasTrait($modelInstance, 'SoftDeletes')){
				$condition = $modelInstance::getStatusFilter();

				$where[] = $condition;
			}

			if($isCRUD && $onlyOwnerReads){
				$primaryKey = $modelInstance::getPrimaryKeyName();
				$columns = DB::EscapeField($tableName) . '.*';
				$from .= sprintf(' JOIN `_api_entity` AS `_ae` ON `_ae`.`entity_id` = %s.%s', DB::EscapeField($tableName), DB::EscapeField($primaryKey));
				$where[] = sprintf('`_ae`.`model` = %s AND `_ae`.`owner_id` = %s', DB::EscapeValue($params->_model), DB::EscapeValue($this->getOwnerId()));
				$groupBy = sprintf('%s.%s', DB::EscapeField($tableName), DB::EscapeField($primaryKey)); // Prevents duplicate results when records are manually deleted from the database but remain in the _api_entity table
			}

			// If model has an API query filter
			if(method_exists($modelInstance, 'API_Query')){
				$this->_verifyAPIFiltersInterface($modelInstance);

				/*
				NOTE: The API_Query() static method in the model class definition should have the following parameters all received by reference (& operator):
				-----
				class modelName extends Model {
					public static function API_Query(object $params, array &$columns, string &$from, array &$where, array &$having, string &$order, string &$limit){
						// Modify parameters here to alter API results
					}
				}
				-----
				*/
			
				// Run query filter
				$modelInstance::API_Query($params, $columns, $from, $where, $having, $order, $limit);
			}

			// Create SQL query
			if(is_string($columns)){ // Allows the "columns" variable to be set as a string in the API filter methods
				$columns = array($columns);
			}

			$sql = "SELECT " . join(', ', $columns) . " FROM " . DB::EscapeField($from);

			if(is_string($where)){ // Allows the "where" variable to be set as a string in the API filter methods
				$where = array($where);
			}

			if(count($where) != 0){
				$sql .= ' WHERE ' . implode(' AND ', $where);
			}

			if(is_string($having)){ // Allows the "having" variable to be set as a string in the API filter methods
				$having = array($having);
			}

			if(count($having) != 0){
				$sql .= ' HAVING ' . join(' AND ', $having);
			}

			if($groupBy){
				$sql .= ' GROUP BY ' . $groupBy;
			}

			if($order){
				$sql .= " ORDER BY {$order}";
			}

			if($limit){
				$sql .= " LIMIT {$limit}";
			}

			if($offset){
				$sql .= " OFFSET {$offset}";
			}

			// Load record set from SQL query
			$recordSet = DB::FindBySql($modelName, $sql);

			if($recordSet){
				// Build response as an array of model records
				$results = array();

				foreach($recordSet as $record){
					/* Process upload fields to return the upload URL in the field rather than the actual raw content */
					if($uploadFields){
						foreach($uploadFields as $fieldName){
							if($url = $record->getUploadUrl($fieldName)){
								$record->{$fieldName} = $url;
							}
							else{
								$record->{$fieldName} = null;
							}
						}
					}

					/* Process multiupload fields to return the upload URL in the field rather than the actual raw content */
					if($multiUploadFields){
						foreach($multiUploadFields as $fieldName){
							if($urls = $record->getMultiUploadUrls($fieldName)){
								$record->{$fieldName} = $urls;
							}
							else{
								$record->{$fieldName} = array();
							}
						}
					}

					/* Process image upload fields to return the resized image URLs in the field rather than the actual raw content */
					if($imageUploadFields){
						foreach($imageUploadFields as $fieldName){
							$originalImageUrl = $record->getUploadUrl($fieldName);
							$largeImageUrl = $record->getLargeImageUrl($fieldName);
							$mediumImageUrl = $record->getMediumImageUrl($fieldName);
							$smallImageUrl = $record->getSmallImageUrl($fieldName);

							$record->{$fieldName} = new stdClass;
							$record->{$fieldName}->original = $originalImageUrl;

							if($record->shouldCreateLargeImage($fieldName))
								$record->{$fieldName}->large = $largeImageUrl;

							if($record->shouldCreateMediumImage($fieldName))
								$record->{$fieldName}->medium = $mediumImageUrl;

							if($record->shouldCreateSmallImage($fieldName))
								$record->{$fieldName}->small = $smallImageUrl;
						}
					}

					/* Process image multiupload fields to return the resized image URLs in the field rather than the actual raw content */
					if($imageMultiUploadFields){
						foreach($imageMultiUploadFields as $fieldName){
							if($count = $record->getUploadCount($fieldName)){
								$images = array();

								for($i=0; $i<$count; $i++){
									$fieldNameWithUploadIndex = "{$fieldName}[{$i}]";
									$originalImageUrl = $record->getUploadUrl($fieldNameWithUploadIndex);
									$largeImageUrl = $record->getLargeImageUrl($fieldNameWithUploadIndex);
									$mediumImageUrl = $record->getMediumImageUrl($fieldNameWithUploadIndex);
									$smallImageUrl = $record->getSmallImageUrl($fieldNameWithUploadIndex);

									$image = new stdClass;
									$image->original = $originalImageUrl;

									if($record->shouldCreateLargeImage($fieldName))
										$image->large = $largeImageUrl;

									if($record->shouldCreateMediumImage($fieldName))
										$image->medium = $mediumImageUrl;

									if($record->shouldCreateSmallImage($fieldName))
										$image->small = $smallImageUrl;

									$images[] = $image;
								}

								$record->{$fieldName} = $images;
							}
							else{
								$record->{$fieldName} = array();
							}
						}
					}

					if($parents){
						foreach($parents as $foreignKey => $parentModel){
							if(is_array($parentModel)){
								$fields = $parentModel[1];
								$fieldName = $parentModel[2] ?: $parentModel[0];
								$parentModel = $parentModel[0];
							} else {
								$fieldName = $parentModel;
								$fields = null;
							}

							if(!$fields){
								$fields = property_exists($parentModel, 'api') && $parentModel::$api['fields'] ? $parentModel::$api['fields'] : null;
							}

							$ids = Util::parseIDList($record->{$foreignKey});
							$integerColumn = (stripos($tableColumns[$foreignKey]['Type'], 'int') !== false);

							if($ids){
								$parentModelColumns = DB::Columns($parentModel);
								$primaryKey = $parentModel::getPrimaryKeyName();

								if($integerColumn){
									$parentResults = $parentModel::findAll(array($primaryKey => $ids[0]))->toArray();
								}
								else{
									$parentResults = $parentModel::findAll(array($primaryKey => array('in', array($ids))))->toArray();
								}

								foreach($parentResults as &$item){
									$this->_prepareRecord($item, $parentModelColumns, $fields);
								}
							}
							else{
								$parentResults = null;
							}

							if($parentResults){
								if(count($parentResults) == 1 && $integerColumn){
									$record->{$fieldName} = $parentResults[0];
								}
								else{
									$record->{$fieldName} = $parentResults;
								}
							}
							else {
								$record->{$fieldName} = null;
							}

							unset($record->{$foreignKey});
						}
					}

					// If model has an API item filter
					if(method_exists($modelInstance, 'API_Item')){
						$this->_verifyAPIFiltersInterface($modelInstance);

						/*
						NOTE: The API_Item() static method in the model class definition should have the $record parameter passed by reference (& operator)
						-----
						class modelName extends Model {
							public static function API_Item(object &$record, object $params){
								// Modify $record object here or return false to remove this item from the results
							}
						}
						-----
						*/
						
						// Apply item filter method
						$returnValue = $modelInstance::API_Item($record, $params);

						// If API_Item() call returns false don't add the item to the results
						if($returnValue === false) continue;
					}

					$this->_prepareRecord($record, $tableColumns, $fieldsToReturn, $modelProperties);

					$results[] = $record;
				}

				// If model has an API results filter
				if(method_exists($modelInstance, 'API_Results')){
					$this->_verifyAPIFiltersInterface($modelInstance);

					/*
					NOTE: The API_Results() static method in the model class definition should have the $results parameter passed by reference (& operator)
					-----
					class modelName extends Model {
						public static function API_Results(array &$results, object $params){
							// Modify $results array here
						}
					}
					-----
					*/
				
					// Run results filter method
					$modelInstance::API_Results($results, $params);
				}

				if($this->isCacheEnabled()){
					/*
					Prepare results for cache storage using serialize(), by converting the model entities to plain objects, otherwise
					the cached response will contain entities with empty (null) properties when the original response didn't
					include those properties (e.g. when using the "fields" API configuration option in the model)
					*/
					foreach($results as $key => $entity){
						$object = new stdClass;

						foreach($entity as $field => $value){
							$object->{$field} = $value;
						}

						$results[$key] = $object;
					}
				}

				if($outputFormat == 'json')
					$this->useJSON();
				elseif($outputFormat == 'xml')
					$this->useXML();

				$response = (object) array(
					'results' => $results
				);

				if($debug){
					$this->includeDebugInfo([
						'sqlQuery' => $sql
					]);
				}

				// If model has an API response postprocessor filter
				if(method_exists($modelInstance, 'API_Response')){
					$this->_verifyAPIFiltersInterface($modelInstance);

					/*
					NOTE: The API_Results() static method in the model class definition should have the $results parameter passed by reference (& operator)
					-----
					class modelName extends Model {
						public static function API_Response(object &$response, object $params){
							// Modify $response object here
						}
					}
					-----
					*/
				
					// Run results filter method
					$modelInstance::API_Response($response, $params);
				}

				// Send response
				$this->send($response);
			}
			else{
				$errorMessage = 'An SQL error ocurred';
				
				if($debug){
					$this->error($errorMessage, array(
						'debug' => array(
							'sqlQuery' => $sql,
							'sqlError' => DB::GetLastError()
						)
					));
				}
				else{
					$this->error($errorMessage);
				}
			}
		}
		else $this->invalidRequest();
	}

	private function _prepareRecord(&$record, $tableColumns = array(), $fieldsToReturn = array(), $modelProperties = array()){
		/*
		1. Exclude fields that match the following criteria:
		- If the "fields" API config list is not empty, those not included in the list
		- Those declared as protected/private properties in the model (automatic OOP behavior as we're loading each record as a Model instance)

		2. Convert numbers to "float" values
		*/

		if (!$fieldsToReturn) $fieldsToReturn = [];

		$fieldsToReturnCount = count($fieldsToReturn);

		foreach($record as $field => $value){
			if(($fieldsToReturnCount == 0 || in_array($field, $fieldsToReturn))){
				if($fieldType = $tableColumns[$field]['Type']){
					if(preg_match('/(int|tinyint|mediumint|bigint|float|double|decimal)/i', $fieldType)){
						$record->{$field} = (float) $value;
					}
				}
				elseif(is_numeric($value) && !is_infinite($value)){
					$record->{$field} = (float) $value;
				}
			}
			else{
				unset($record->{$field});
			}
		}
	}

	private function _verifyAPIFiltersInterface($modelInstance){
		if(!($modelInstance instanceof Filters)){
			throw new Exception(_T("APIRequestController: The model \"%s\" needs to implement the API\Filters interface in order to use the API request filtering methods.", $modelInstance));
		}
	}

	private function assertSafeParameter($input){
		if($input && !Util::isSafeSQLFilter($input)){
			throw new Exception(_T('APIRequestController: The value "%s" is not a safe SQL parameter.', $input));
		}
	}
}