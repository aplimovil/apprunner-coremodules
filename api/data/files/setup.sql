-- --------------------------------------------------------

--
-- Table structure for table `_api_entity`
--

CREATE TABLE IF NOT EXISTS `_api_entity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `owner_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_time` int(11) NOT NULL,
  `updated_time` int(11) DEFAULT NULL,
  `deleted_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `created_time` (`created_time`),
  KEY `updated_time` (`updated_time`),
  KEY `deleted_time` (`deleted_time`),
  KEY `model` (`model`,`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_api_login`
--

CREATE TABLE `_api_login` (
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `login_time` int(10) unsigned DEFAULT NULL,
  `login_ip_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fail_time` int(10) unsigned DEFAULT NULL,
  `fail_count` int(11) DEFAULT NULL,
  `fail_ip_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `model` (`model`),
  KEY `entity_id` (`entity_id`),
  KEY `fail_time` (`fail_time`),
  KEY `fail_ip_address` (`fail_ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
