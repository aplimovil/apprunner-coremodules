<?php
class CoreApi_ApiCreateController extends API\AbstractCRUDController {
	function main($APP, $GET, $POST){
		$this->init(API\ACTION_CREATE, $GET, $POST);

		$modelName = $this->modelName;
		$entity = new $modelName;

		foreach($POST as $field => $value){
			if(property_exists($entity, $field)){
				$entity->{$field} = trim($value);
			}
		}

		try{
			DB::Begin();

			if(method_exists($entity, 'onAPICreate')){
				$result = $entity->onAPICreate($entity, $POST, true);
				if($result === false) $this->error(_T('API create cancelled'));
			}

			$entity->save();

			$this->createOwnership($entity);

			if(method_exists($entity, 'onAPICreate')){
				$entity->onAPICreate($entity, $POST, false);
			}

			DB::Commit();
		}
		catch(ValidationException $e){
			$this->error($e->getMessage());
		}
		catch(API\Exception $e){
			$this->error($e->getMessage());
		}

		$this->send([
			'entity' => $entity->getData(true, $this->getFields()),
			'_OWNER_ID' => $this->getOwnerId(),
			'_OWNER_TOKEN' => $this->getOwnerToken()
		]);
	}
}