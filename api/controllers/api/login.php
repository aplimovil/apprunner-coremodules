<?php
/*
Sample configuration settings for "api" static property in model:

	static $api = [
		'login' => [bool], // default: false / whether to allow the login API on this model
		'usernameField' => [string], // default: "username"
		'passwordHashField' => [string], // default: "password_hash"
		'fields' => [array], // Only return this fields from the model
		'failLimit' => [number], // default: 5 / maximum number of login failures per account before locking the account for failLockoutMinutes
		'failLockoutMinutes' => [number], // default: 5 / number of minutes to lock an account when failLimit has been reached
		'failWaitSeconds' => [number], // default: 3 / number of seconds that the user should wait before trying to login again after a failed attempt
		'ipAddressMaxDailyFails' => [number], // default: 10 / maximum number of login failures to allow from an IP address in the last 24 hours, for any account, before blacklisting
		'ipAddressMaxTotalFails' => [number], // default: 100 / maximum number of login failures to allow from an IP address in all time, for any account, before blacklisting
		'purgeAfterDays' => [number], // default: 365 / delete login records older than these number of days
		'debug' => [bool], // default: false
		'useTextStatus' => [bool],
		'errorField' => [string],
		'public' => [bool] // default: false
	];

NOTES:
- This API endpoint receives the "_USERNAME" and "_PASSWORD" parameters via POST
*/

class CoreApi_ApiLoginController extends APIController {
	function main($APP, $GET, $POST){
		$modelName = $GET->_model;

		if($APP->modelExists($modelName)){
			$modelClassName = '\\' . $modelName;

			if(property_exists($modelClassName, 'api')){
				if($modelClassName::$api === false){
					$this->invalidRequest();
				}

				if($modelClassName::$api['login'] != true){
					$this->error(_T('LOGIN operation not allowed on model'));
				}

				if(isset($modelClassName::$api['debug'])){
					$this->debug = $modelClassName::$api['debug'];
					if($this->debug) $this->includeDebugInfo();
				}

				if($modelClassName::$api['useTextStatus']){
					$this->useTextStatus();
				}

				if(isset($modelClassName::$api['errorField'])){
					$this->setErrorField($modelClassName::$api['errorField']);
				}

				if(!$modelClassName::$api['public']){
					$this->authorize();
				}

				$username = trim($POST->_USERNAME);
				$password = trim($POST->_PASSWORD);

				$usernameField = 'username';
				$passwordHashField = 'password_hash';
				$fields = null;
				$failLimit = 5;
				$failLockoutMinutes = 5;
				$failWaitSeconds = 3;
				$ipAddressMaxDailyFails = 10;
				$ipAddressMaxTotalFails = 100;
				$purgeAfterDays = 365;

				if(isset($modelClassName::$api['usernameField'])){
					$usernameField = $modelClassName::$api['usernameField'];
				}

				if(isset($modelClassName::$api['passwordHashField'])){
					$passwordHashField = $modelClassName::$api['passwordHashField'];
				}

				if(isset($modelClassName::$api['fields']) && is_array($modelClassName::$api['fields'])){
					$fields = $modelClassName::$api['fields'];
				}

				if(isset($modelClassName::$api['failLimit']) && is_numeric($modelClassName::$api['failLimit'])){
					$failLimit = $modelClassName::$api['failLimit'];
				}

				if(isset($modelClassName::$api['failLockoutMinutes']) && is_numeric($modelClassName::$api['failLockoutMinutes'])){
					$failLockoutMinutes = $modelClassName::$api['failLockoutMinutes'];
				}

				if(isset($modelClassName::$api['failWaitSeconds']) && is_numeric($modelClassName::$api['failWaitSeconds'])){
					$failWaitSeconds = $modelClassName::$api['failWaitSeconds'];
				}

				if(isset($modelClassName::$api['ipAddressMaxDailyFails']) && is_numeric($modelClassName::$api['ipAddressMaxDailyFails'])){
					$ipAddressMaxDailyFails = $modelClassName::$api['ipAddressMaxDailyFails'];
				}

				if(isset($modelClassName::$api['ipAddressMaxTotalFails']) && is_numeric($modelClassName::$api['ipAddressMaxTotalFails'])){
					$ipAddressMaxTotalFails = $modelClassName::$api['ipAddressMaxTotalFails'];
				}

				if(isset($modelClassName::$api['purgeAfterDays']) && is_numeric($modelClassName::$api['purgeAfterDays'])){
					$purgeAfterDays = $modelClassName::$api['purgeAfterDays'];
				}

				if(!$username) $this->error(_T('Username is required'), ['code' => 'usernameEmpty']);

				$filter = array();
				$filter[$usernameField] = $username;

				$this->purgeOldRecords($purgeAfterDays);
				$this->checkForBlacklisting($ipAddressMaxDailyFails, $ipAddressMaxTotalFails);

				if($account = $modelClassName::findOne($filter)){
					if($loginRecord = DB::GetValues('SELECT * FROM _api_login WHERE model = ? AND entity_id = ?', [$modelName, $account->getId()])){
						$failLockoutSeconds = $failLockoutMinutes * 60;
						$failSecondsDiff = time() - $loginRecord['fail_time'];
						$failCount = $loginRecord['fail_count'];

						if($failCount >= $failLimit){
							if($failSecondsDiff < $failLockoutSeconds){
								$this->error(_T('For your security your account has been locked out for %s minutes due to exceeding the maximum number of failed login attempts. Please wait and try again.', $failLockoutMinutes), ['code' => 'lockout', 'lockoutMinutes' => $failLockoutMinutes]);
							}
						}
						else{
							if($failSecondsDiff < $failWaitSeconds){
								$this->error(_T('Please wait before trying again.'), ['code' => 'wait']);
							}
						}
					}
					else{
						DB::Query('INSERT INTO _api_login (model, entity_id, fail_count, fail_time) VALUES(?, ?, 0, 0)', [$modelName, $account->getId()]);
						$failCount = 0;
					}

					$hash = $account->{$passwordHashField};

					if(!$password) $this->error(_T('Password is required'), ['code' => 'passwordEmpty']);

					if(Util::verifyPassword($password, $hash)){
						if(isset($account->status)){
							if($account->status != STATUS_ACTIVE){
								$this->error(_T('Account is not active'), ['code' => 'accountNotActive']);
							}
						}

						DB::Query('UPDATE _api_login SET login_time = ?, login_ip_address = ?, fail_count = 0 WHERE model = ? AND entity_id = ?', [time(), Util::getRealIPAddress(), $modelName, $account->getId()]);

						$this->send([
							'account' => $account->getData(true, $fields)
						]);
					}
					else{
						DB::Query('UPDATE _api_login SET fail_count = fail_count + 1, fail_time = ?, fail_ip_address = ? WHERE model = ? AND entity_id = ?', [time(), Util::getRealIPAddress(), $modelName, $account->getId()]);

						$this->error(_T('Your login credentials are not valid, please try again.'), ['code' => 'invalidCredentials']);
					}
				}
				else $this->error(_T('Your login credentials are not valid, please try again.'), ['code' => 'invalidCredentials']);
			}
			else $this->invalidRequest();
		}
		else $this->invalidRequest();
	}

	protected function purgeOldRecords($afterDays){
		if(mt_rand(0, 100) <= 5){ // Purge only once on approximately 5% of requests to not affect performance
			$maxTime = time() - (3600 * 24 * $afterDays);
			DB::Query('DELETE FROM _api_login WHERE (login_time != 0 AND login_time < ?) OR (fail_time != 0 AND fail_time < ?)', [$maxTime, $maxTime]);
		}
	}

	protected function checkForBlacklisting($maxDailyFails, $maxTotalFails){
		$ipAddress = Util::getRealIPAddress();

		$startTime = time() - (3600 * 24);
		$dayFailCount = DB::GetValue('SELECT SUM(fail_count) FROM _api_login WHERE fail_ip_address = ? AND fail_time >= ?', [$ipAddress, $startTime]);

		if($dayFailCount > $maxDailyFails){
			$this->sendBlacklistedError();
		}

		$totalFailCount = DB::GetValue('SELECT SUM(fail_count) FROM _api_login WHERE fail_ip_address = ?', [$ipAddress]);

		if($totalFailCount > $maxTotalFails){
			$this->sendBlacklistedError();
		}
	}

	private function sendBlacklistedError(){
		$this->error(_T('Your IP address has been blacklisted.'), ['code' => 'blacklisted']);
	}
}