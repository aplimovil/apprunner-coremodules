<?php
class CoreApi_ApiDeleteController extends API\AbstractCRUDController {
	function main($APP, $GET, $POST){
		$this->init(API\ACTION_DELETE, $GET, $POST);

		$modelName = $this->modelName;
		$entity = $modelName::get($this->getEntityId());

		if($entity){
			foreach($POST as $field => $value){
				if(property_exists($entity, $field)){
					$entity->{$field} = $value;
				}
			}

			try{
				DB::Begin();

				if(method_exists($entity, 'onAPIDelete')){
					$result = $entity->onAPIDelete($entity, $POST, true);
					if($result === false) $this->error(_T('API delete cancelled'));
				}

				$entity->delete();

				$this->registerDelete();

				if(method_exists($entity, 'onAPIDelete')){
					$entity->onAPIDelete($entity, $POST, false);
				}

				DB::Commit();
			}
			catch(API\Exception $e){
				$this->error($e->getMessage());
			}

			$this->send();
		}
		else $this->error(_T('Entity not found'));
	}
}