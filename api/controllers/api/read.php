<?php
class CoreApi_ApiReadController extends API\AbstractRequestController {
	use API\CRUD;

	function main($APP, $GET, $POST){
		$this->action = API\ACTION_READ;
		$this->modelName = $GET->_model;

		$this->prepare();
		$this->loadParams($GET);
		$this->verifyOwner();

		// Ownership verificiation is done inside API\AbstractRequestController->run() by detecting the $onlyOwnerReads parameter
		$this->run($GET, true, $this->onlyOwnerReads);
	}
}