<?php
class CoreApi_ApiUpdateController extends API\AbstractCRUDController {
	function main($APP, $GET, $POST){
		$this->init(API\ACTION_UPDATE, $GET, $POST);

		$modelName = $this->modelName;
		$entity = $modelName::get($this->getEntityId());

		if($entity){
			foreach($POST as $field => $value){
				if(property_exists($entity, $field)){
					$entity->{$field} = trim($value);
				}
			}

			try{
				$originalState = $modelName::get($entity->getId());

				foreach($this->getLockedFields() as $fieldName){
					if($originalState->{$fieldName} != $entity->{$fieldName}){
						throw new API\Exception(_T('The field "%s" cannot be modified', $fieldName));
					}
				}

				DB::Begin();

				if(method_exists($entity, 'onAPIUpdate')){
					$result = $entity->onAPIUpdate($entity, $POST, true);
					if($result === false) $this->error(_T('API update cancelled'));
				}

				$entity->save();

				$this->registerUpdate();

				if(method_exists($entity, 'onAPIUpdate')){
					$entity->onAPIUpdate($entity, $POST, false);
				}

				DB::Commit();
			}
			catch(ValidationException $e){
				$this->error($e->getMessage());
			}
			catch(API\Exception $e){
				$this->error($e->getMessage());
			}

			$this->send([
				'entity' => $entity->getData(true, $this->getFields())
			]);
		}
		else $this->error(_T('Entity not found'));
	}
}