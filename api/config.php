<?php
namespace API;

use APP, ErrorLogger, API, Util, Lang;

const ACTION_GET = 'get';
const ACTION_CREATE = 'create';
const ACTION_READ = 'read';
const ACTION_UPDATE = 'update';
const ACTION_DELETE = 'delete';

APP::routes(array(
	'api/get/(.*)' => 'api/request?_model=$1',
	'api/create/(.*)' => 'api/create?_model=$1',
	'api/read/(.*)' => 'api/read?_model=$1',
	'api/update/(.*)' => 'api/update?_model=$1',
	'api/delete/(.*)' => 'api/delete?_model=$1',
	'api/login/(.*)' => 'api/login?_model=$1'
));

APP::onConfigLoad(function($APP){
	Lang::load('api');
});

APP::onInvalidRoute(function(){
	$api = new API;
	$api->invalidRequest();
});

\APIController::HandleErrors();