<?php
namespace Geo;

use Util;
use Exception;

trait Mapping {
	public $country_id;
	public $state_id;
	public $city_id;

	// static $autoUpdateAddress = true; // Uncomment when PHP supports trait property overriding

	private $_autoUpdateAddress = true;
	private $_previousState;
	private $_newAddress = false;
	
	function onInsert(){
		if(property_exists(get_called_class(), 'autoUpdateAddress') && self::$autoUpdateAddress === false) return;

		if($this->_autoUpdateAddress && property_exists($this, 'address')){
			$this->_newAddress = true;
			$this->updateLocation();
		}
	}

	function onBeforeUpdate(){
		if(property_exists(get_called_class(), 'autoUpdateAddress') && self::$autoUpdateAddress === false) return;

		$model = get_class($this);
		$this->_previousState = $model::get($this->getId());
	}

	function onUpdate(){
		if(property_exists(get_called_class(), 'autoUpdateAddress') && self::$autoUpdateAddress === false) return;

		if($this->_autoUpdateAddress && property_exists($this, 'address')){
			if($this->_previousState->getFullAddress() != $this->getFullAddress()){
				$this->_newAddress = true;
				$this->updateLocation();
			}
		}
	}

	function getAutoUpdateAddress(){
		return $this->_autoUpdateAddress;
	}

	function setAutoUpdateAddress($value){
		$this->_autoUpdateAddress = $value;
	}

	function hasNewAddress(){
		return $this->_newAddress;
	}

	function getMapRoute(){
		return self::$route . '/{id}/map';
	}

	function goToMap($always = true, $route = null){
		global $APP;

		if(!$route){
			$route = $this->getMapRoute();
		}

		$located = ($this->latitude && $this->longitude) ? 1 : 0;

		if($always || $this->hasNewAddress()){
			$APP->redirect('/' . Util::replaceTags($route, ['id' => $this->getId()]) . "?_located={$located}");
		}
	}

	function getFullAddress(){
		$fullAddress = array();

		if(trim($this->address) != ''){
			$fullAddress[] = trim($this->address);
		}

		if($city = $this->findParent('city')){
			$fullAddress[] = $city->name;

			if($state = $city->findParent('state')){
				$fullAddress[] = $state->name;

				if($country = $state->findParent('country')){
					$fullAddress[] = $country->name;
				}
			}
		}

		return join(', ', $fullAddress);
	}

	function updateLocation($latitude = 0, $longitude = 0){
		if(!Util::classHasTrait($this, 'Geo\Location')){
			throw new Exception(_T('The class "%s" requires the Geo\Location trait in order to use the Geo\Mapping trait.', get_class($this)));
		}

		if($latitude || $longitude){
			$this->setGeolocation($latitude, $longitude);
		}
		else{
			$address = $this->getFullAddress();

			if($georesult = Util::geocode($address)){
				$this->setGeolocation($georesult->latitude, $georesult->longitude);
			}
		}

		return $this;
	}
}