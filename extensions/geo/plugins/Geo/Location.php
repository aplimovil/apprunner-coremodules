<?php
namespace Geo;

use DB, Util;
use Exception;

trait Location {
	/* Fields */
	public $latitude;
	public $longitude;
	protected $geolocation;

	/* Properties */
	public $distance;

	static $fieldsExcludedFromSave = 'geolocation';

	function getGeolocationFieldName(){
		return (property_exists(get_called_class(), 'geolocationField') ? static::$geolocationField : 'geolocation');
	}

	function getGeolocation(){
		return $this->geolocation;
	}

	function setGeolocation($latitude = 0, $longitude = 0){
		$geolocationFieldName = $this->getGeolocationFieldName();

		if($latitude && $longitude){
			$result = $this->set([
				'latitude' => $latitude,
				'longitude' => $longitude
			]);

			if($this->save()){
				// OK
			}
			else{
				throw new Exception(_T("The record's coordinates couldn't be updated."));
			}
		}

		$primaryKey = DB::EscapeField($this->getPrimaryKeyName());
		$query = "UPDATE " . DB::EscapeField($this->getTableName()) . " SET " . DB::EscapeField($geolocationFieldName) . " = PointFromText('POINT({$this->latitude} {$this->longitude})') WHERE {$primaryKey} = " . DB::EscapeValue($this->getId());
		$result = DB::Query($query);

		if($result){
			return true;
		}
		else{
			throw new Exception(_T("The record's geolocation field (%s) couldn't be updated", $geolocationFieldName));
		}
	}

	static function searchByRadius($latitude, $longitude, $radius = 0, $where = null, $order = "`distance` ASC", $limit = 0){ // Radius in kilomters
		global $APP;

		if(is_numeric($latitude) && is_numeric($longitude)){
			$modelClass = get_called_class();
			$tableName = DB::EscapeField(DB::Class2Table($modelClass));
			$geolocationFieldName = DB::EscapeField(self::getGeolocationFieldName());

			if($APP->settings->DB_ENGINE == 'mysql'){
				$query = "SELECT *, (glength(linestringfromwkb(linestring({$geolocationFieldName}, PointFromText('POINT({$latitude} {$longitude})')))) * 111.16581) AS `distance` FROM {$tableName}";
				
				if(is_array($where)){
					$where = Util::arrayToSQLFilter($where);
				}

				$box = Util::getGeoBoundingBox($latitude, $longitude, $radius);
				$where = ($where ? "{$where} AND {$geolocationFieldName} IS NOT NULL AND " : "") . sprintf('(latitude BETWEEN %s and %s AND longitude BETWEEN %s and %s)', $box->SW->latitude, $box->NW->latitude, $box->NW->longitude, $box->NE->longitude);

				if(Util::classHasTrait(get_called_class(), 'SoftDeletes')){
					$where = static::appendStatusFilter($where);
				}

				if($where && is_string($where)){
					$query .= " WHERE {$where}";
				}

				if($radius){
					$query .= " HAVING `distance` <= {$radius}";
				}
			}
			elseif($APP->settings->DB_ENGINE == 'postgresql'){
				throw new Exception("PostgreSQL GIS search not implemented yet.");
			}
			else throw new Exception("Current DB_ENGINE doesn't support GIS search.");

			if($order) $query .= " ORDER BY {$order}";
			if($limit) $query .= " LIMIT {$limit}";

			$results = DB::FindBySql($modelClass, $query);

			return $results;
		}
		else throw new Exception("Coordinates given are not numeric.");
	}

	static function searchByPolygon($vertices, $order = false, $limit = 0, $appendFirstVertex = true){
		global $APP;

		$modelClass = get_called_class();
		$serverVersion = DB::Connection()->server_info;
		$tableName = DB::Class2Table($modelClass);
		$geolocationFieldName = self::getGeolocationFieldName();
		$verticesArray = array();

		if(is_array($vertices)){
			if(count($vertices) >= 3){
				if($appendFirstVertex) $vertices[] = $vertices[0];

				foreach($vertices as $point){
					$pointString = join(' ', $point);
					$verticesArray[] = $pointString;
				}

				$verticesString = join(', ', $verticesArray);

				if($APP->settings->DB_ENGINE == 'mysql'){
					if(preg_match("/^5\.6\./", $serverVersion)){ // mySQL 5.6
						$query = "SELECT * FROM {$tableName} WHERE ST_Within({$geolocationFieldName}, GeomFromText('Polygon(({$verticesString}))'))";
					}
					else{ // mySQL 5.5-
						$query = "SELECT * FROM {$tableName} WHERE MBRWithin({$geolocationFieldName}, GeomFromText('Polygon(({$verticesString}))'))";
					}
				}
				elseif($APP->settings->DB_ENGINE == 'postgresql'){
					throw new Exception("PostgreSQL GIS search not implemented yet.");
				}
				else throw new Exception("Current DB_ENGINE doesn't support GIS search.");

				if($order) $query .= " ORDER BY {$order}";
				if($limit) $query .= " LIMIT {$limit}";

				$results = DB::FindBySql($modelClass, $query);

				return $results;
			}
			else throw new Exception("The polygon should have be at least 3 vertices.");
		}
		else throw new Exception("Vertices should be an array of latitude/longitude array pairs.");
	}
}