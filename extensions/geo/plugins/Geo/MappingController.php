<?php
namespace Geo;

use Util, Exception;

abstract class MappingController extends \Controller {
	protected $place;
	protected $hasAddress = true;

	protected function init($place, $hasAddress = true){
		if(!$place){
			$this->error(_T('Lugar no encontrado.'));
			return false;
		}

		if(!Util::classHasTrait($place, 'Geo\Location')){
			throw new Exception(_T('The class "%s" requires the Geo\Location trait in order to use Geo\MappingController.', get_class($place)));
		}
		elseif(!Util::classHasTrait($place, 'Geo\Mapping')){
			throw new Exception(_T('The class "%s" requires the Geo\Mapping trait in order to use Geo\MappingController.', get_class($place)));
		}

		$this->place = $place;
		$this->hasAddress = $hasAddress;

		$this->setTitle(_T('%s: Ubicación en Mapa', $place->getName()));

		if($_POST['cmd'] == 'update'){
			$this->updateLocation();
		}
		elseif($_POST['cmd'] == 'reset'){
			$this->resetLocation();
		}
		else{
			$this->run();
		}
	}

	protected function run(){
		if(isset($_GET['_located'])){
			if($_GET['_located'] == '1'){
				$this->message(_T('Por favor revise la ubicación del lugar en el mapa para confirmar que este correcta.'));
			}
			else{
				$this->error(_T('La ubicación del lugar no pudo ser determinada automáticamente. Por favor utilice el mapa para indicar la ubicación.'));
			}
		}
	}

	protected function updateLocation(){
		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];
		
		if(is_numeric($latitude) && is_numeric($longitude)){
			try{
				$this->place->updateLocation($latitude, $longitude);

				$this->message(_T('La ubicación del lugar ha sido actualizada.'));
			}
			catch(Exception $e){
				throw $e;
				//$this->error(_T('No se pudo actualizar la ubicación del lugar (Error: %s)', $e->getMessage()));
			}
		}
		else{
			$this->error(_T('Las coordenadas proporcionadas son inválidas.'));
		}
	}

	protected function resetLocation(){
		try{
			$this->place->updateLocation();
			$this->message(_T('La ubicación del lugar ha sido reestablecida a las coordenadas originales correspondientes a su dirección.'));
		}
		catch(Exception $e){
			$this->error(_T('No se pudo reestablecer la ubicación del lugar (Error: %s)', $e->getMessage()));
		}
	}

	protected function view(){
		$this->loadTemplate('Geo/MappingController/View', [
			'place' => $this->place,
			'hasAddress' => $this->hasAddress
		]);
	}
}