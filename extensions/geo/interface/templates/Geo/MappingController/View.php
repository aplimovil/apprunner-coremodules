<? $this->addBufferToHeader(function() use(&$place){ ?>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3&amp;key=<?= Geo\Config::$googleMapsApiKey ?>"></script>
<script type="text/javascript">
$(function(){
	var position, zoom, placeMap, placeMarker, form;

	mapLocationForm = document.forms.frmGeoMappingControllerMap;
	
	var latitude = <?= $place->latitude ?: 0 ?>;
	var longitude = <?= $place->longitude ?: 0 ?>;
	
	position = new google.maps.LatLng(latitude, longitude);
	
	if(latitude != 0 && longitude != 0){
		zoom = 16;
	}
	else{
		zoom = 3;
	}

	placeMap = new google.maps.Map($("#placeMap")[0], {
		center: position,
		zoom: zoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDoubleClickZoom: true
	});
	
	placeMarker = new google.maps.Marker({
		position: position,
		map: placeMap,
		animation: google.maps.Animation.DROP,
		draggable: true
	});
	
	google.maps.event.addListener(placeMarker, "dragend", function(event){
		var position = placeMarker.getPosition();
		mapLocationForm.latitude.value = position.lat();
		mapLocationForm.longitude.value = position.lng();
	});

	$('#frmGeoMappingControllerMap').find('input[name=latitude], input[name=longitude]').change(function(){
		var form = document.forms.frmGeoMappingControllerMap;
		var latitude = parseFloat(form.latitude.value);
		var longitude = parseFloat(form.longitude.value);

		if(!isNaN(latitude) && !isNaN(longitude)){
			var position = {
				lat: latitude,
				lng: longitude
			};

			placeMap.panTo(position);
			placeMarker.setPosition(position);
		}
	});
	
	$("#btnSaveLocation").click(function(){
		mapLocationForm.cmd.value = "update";
		mapLocationForm.submit();
		
		return false;
	});
	
	$("#btnResetLocation").click(function(){
		if(window.confirm("<?= _T('¿Esta seguro que desea reestablecer la ubicación a la proporcionada por el geocodificador para la dirección del lugar?') ?>")){
			mapLocationForm.cmd.value = "reset";
			mapLocationForm.submit();
		}
		return false;
	});
});
</script>
<? }); ?>
<div style="width: 700px;">
	<p>
		<span style="font-size: 14px; font-weight: bold;"><?= _T('Instrucciones') ?>: </span><br />
		<?= Util::htmlentities(_T('Arrastre el marcador hacia la ubicación geográfica deseada y de clic en "Guardar Ubicación" para cambiarla.')) ?>
		<? if($hasAddress): ?>
		<?= Util::htmlentities(_T('Si desea restablecer la ubicación del lugar a las coordenadas originales correspondientes a su dirección, de clic en "Reestablecer Ubicación".')) ?>
		<? endif; ?>
	</p>

	<a href="<?= $this->path($place::$route) ?>" class="button icon back" style="margin-right: 5px;"><?= _T('Regresar') ?></a>
	<a href="#" id="btnSaveLocation" class="button icon submit" style="margin-right: 5px;"><?= _T('Guardar Ubicación') ?></a>
	<? if($hasAddress): ?>
	<a href="#" id="btnResetLocation" class="button icon reset"><?= _T('Reestablecer Ubicación') ?></a>
	<? endif; ?>
	
	<form name="frmGeoMappingControllerMap" id="frmGeoMappingControllerMap" action="<?= $this->getPath() ?>" method="POST" style="margin-top: 10px;">
		<input type="hidden" name="cmd" value="update" />
		<span style="font-weight: bold;"><?= _T('Latitud') ?>: <input type="text" name="latitude" size="20" value="<?= $place->latitude ?>" /></span>
		<span style="margin-left: 5px; font-weight: bold;"><?= _T('Longitud') ?>: <input type="text" name="longitude" size="20" value="<?= $place->longitude ?>" /></span>
		<? if($hasAddress): ?>
		<p style="margin: 5px 0px 0px 0px; padding: 0px;">
		  <span style="font-weight: bold;"><?= _T('Dirección') ?>:</span><br />
		  <span><a href="<?= $place->getCatalogLink(null, true) ?>"><?= $place->getFullAddress() ?></a></span>
		</p>
		<? endif; ?>
	</form>
	
	<div id="placeMap" style="height: 400px; margin: 15px 0px; border: 1px inset;"></div>
</div>