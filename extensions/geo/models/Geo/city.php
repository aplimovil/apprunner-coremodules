<?php
namespace Geo;

class city extends \Model
{
	use \Entity, \SoftDeletes, \Validation;

	static $route = 'admin/catalogs/cities';
	static $api = [
		'cache' => true
	];

	public $id;
	public $state_id;
	public $name;
	protected $status;

	function getNameWithState()
	{
		$state = $this->findParent('state');
		return $this->name . ', ' . $state->name;
	}
}