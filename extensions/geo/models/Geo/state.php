<?php
namespace Geo;

class state extends \Model
{
	use \Entity, \SoftDeletes, \Validation;

	static $route = 'admin/catalogs/states';
	static $api = [
		'cache' => true
	];

	public $id;
	public $name;
	protected $status;
}