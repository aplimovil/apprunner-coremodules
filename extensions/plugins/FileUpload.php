<?php
class FileUpload {
	public $id;
	public $index;
	public $number;
	public $folder;
	public $name;
	public $original_name;
	public $path;
	public $timestamp;
	public $comments;
	public $abspath;
	public $webpath;
	public $abswebpath;

	function __construct($properties){
		foreach($properties as $property => $value){
			$this->{$property} = $value;
		}
	}

	function __get($property){
		throw new Exception(_T('The property "%s" does not exist in the %s object.', $property, __CLASS__));
	}

	function __set($property, $value){
		throw new Exception(_T('The property "%s" does not exist in the %s object.', $property, __CLASS__));
	}

	function getId(){
		return $this->id;
	}

	function getIndex(){
		return $this->index;
	}

	function getFolder(){
		return $this->folder;
	}

	function getFilename(){
		return $this->name;
	}

	function getOriginalFilename(){
		return $this->original_name;
	}

	function getPath(){
		return $this->path;
	}

	function getFile(){
		return $this->getFolder() . '/' . $this->getFilename();
	}

	function getAbsolutePath(){
		return $this->abspath;
	}

	function getURL($absolute = true){
		return $absolute ? $this->abswebpath : $this->webpath;
	}

	function getPermalink(){
		return $this->getURL();
	}

	function getTimestamp(){
		return $this->timestamp;
	}

	function getComments(){
		return $this->comments;
	}

	function getMIMEType(){
		return Util::getFileMIMEType($this->getFilename());
	}
}