<?php
interface MappingInterface {
	static function getMappedProperty($name);
	static function getReverseMappedProperty($name);
}