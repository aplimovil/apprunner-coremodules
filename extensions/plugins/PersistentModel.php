<?php
/*
@name			Persistent Model (PDB)
@description	Personalized version of the Model class for persistent (non-deletable) records. Uses a "status" column with a value of "0" to mark records as deleted.
*/

// DEPRECATED: Use "SoftDeletes" trait in the model class instead
abstract class PersistentModel extends Model {
	use SoftDeletes;
}