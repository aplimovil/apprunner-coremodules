<?php
trait Uploads {
	// Supports retrieval of a particular upload index from a multi-upload field by adding the index within brackets as a suffix to the upload field name
	public function getUpload($sField){
		$iIndex = 0;

		if(preg_match('/\[([0-9]+)\]$/', $sField, $aMatches)){
			// Retrieve image by index from multiupload field
			$iIndex = (int) $aMatches[1];
			$sField = $this->removeUploadIndexFromFieldName($sField);
		}

		if($aUploadItems = Form::parseUploadField($this->{$sField})){
			return new FileUpload($aUploadItems[$iIndex]);
		}
		else return false;
	}

	public function getUploadUrl($sField, $absolute = true){
		$upload = $this->getUpload($sField);
		return $upload ? $upload->getURL($absolute) : null;
	}

	public function getMultiUpload($sField){
		$aUploadItems = Form::parseUploadField($this->{$sField});
		$aUploads = array();

		if($aUploadItems){
			foreach($aUploadItems as $aItem){
				$aUploads[] = new FileUpload($aItem);
			}

			return $aUploads;
		}
		else return false;
	}

	public function getMultiUploadUrls($sField){
		$files = $this->getMultiUpload($sField);
		$urls = array();

		if($files){
			foreach($files as $file){
				$urls[] = $file->getURL(true);
			}
		}

		return $urls;
	}

	public function isUploadField($sField){
		$value = $this->{$sField};

		if($value && ($aUpload = Form::parseUploadField($value))){
			if($aUpload[0] && $aUpload[0]->name){
				return true;
			}
		}

		return false;
	}

	public function getUploadCount($sField){
		if($aUploads = Form::parseUploadField($this->{$sField})){
			return count($aUploads);
		}
		else return false;
	}

	public function removeUploadIndexFromFieldName($field){
		return preg_replace('/\[([0-9]+)\]$/', '', $field);
	}

	public function setUpload($sField, $sFileName, $sFolder, $sComments = '', $iTimestamp = 0){
		if($this->removeUpload($sField)){
			if($iTimestamp == 0) $iTimestamp = time();

			$field = new stdClass;
			$field->name = $sField;
			$field->type = Form::FIELD_UPLOAD;

			$item = Form::createUploadObjectItem($field, $sFileName, $sFileName, $iTimestamp, $sFolder, $sComments);
			$aUploadItems = array($item);
			$this->{$sField} = json_encode($aUploadItems);

			$this->save();

			return true;
		}
		else{
			return false;
		}
	}

	public function setMultiUpload($sField, $aFiles){
		if($this->removeMultiUpload($sField)){
			$aUploadItems = array();

			foreach($aFiles as $file){
				if(!isset($file->path) || !isset($file->folder)){
					throw new Exception('One or more of the multi-upload files do not have the "path" and/or "folder" attributes.');
				}

				$sFileName = pathinfo($file->path, PATHINFO_BASENAME);

				if(!isset($file->timestamp)) $file->timestamp = time();
				if(!isset($file->comments)) $file->comments = '';

				$item = Form::createUploadObjectItem($sField, $sFileName, $sFileName, $file->timestamp, $file->folder, $file->comments);
				$aUploadItems[] = $item;
			}

			$this->{$sField} = json_encode($aUploadItems);

			return true;
		}
		else{
			return false;
		}
	}

	public function setAmazonS3Upload($sField, $sFileName, $sFolder, $sBucket, $sComments = '', $iTimestamp = 0){
		if($this->removeUpload($sField)){
			if($iTimestamp == 0) $iTimestamp = time();

			$field = new stdClass;
			$field->name = $sField;
			$field->type = Form::FIELD_AMAZON_S3_UPLOAD;
			$field->bucket = $sBucket;

			$item = Form::createUploadObjectItem($field, $sFileName, $sFileName, $iTimestamp, $sFolder, $sComments);
			$aUploadItems = array($item);
			$this->{$sField} = json_encode($aUploadItems);

			$this->save();

			return true;
		}
		else{
			return false;
		}
	}

	public function setUploadFromFile($sField, $sFilePath, $sSaveToFolder, $sComments = '', $iTimestamp = 0){
		global $APP;

		$filePathInfo = (object) pathinfo($sFilePath);
		$sFileName = $filePathInfo->basename;
		
		$sNewFilePath = $APP->path(APP::FOLDER_UPLOADS . '/' . $sSaveToFolder . '/' . $sFileName, true);
		$newFilePathInfo = (object) pathinfo($sNewFilePath);

		if($sFilePath == $sNewFilePath){
			// If the file path passed corresponds to the actual path the file would be saved to then don't do anything
		}
		else{
			// If the file path is different it means we should move the passed file path to the corresponding upload location

			$counter = 1;

			// If the destination file path already exists then create a unique filename by adding a numeric suffix
			while(file_exists($sNewFilePath)){
				$sNewFilePath = $newFilePathInfo->dirname . '/' . $newFilePathInfo->filename . '~' . $counter . '.' . $newFilePathInfo->extension;
				$counter ++;
			}

			if(copy($sFilePath, $sNewFilePath)){
				$sFileName = pathinfo($sNewFilePath, PATHINFO_BASENAME);
			}
			else{
				throw new Exception('The file could not be copied');
			}
		}

		$result = $this->setUpload($sField, $sFileName, $sSaveToFolder, $sComments, $iTimestamp);

		return $result;
	}

	public function setUploadFromUploadedFile($sField, $uploadedFile, $sSaveToFolder, $sComments = '', $iTimestamp = 0){
		if(is_uploaded_file($uploadedFile['tmp_name'])){
			$sTempFilePath = $uploadedFile['tmp_name'];
			$tempFilePathInfo = (object) pathinfo($sTempFilePath);

			// Create a temporary file based in the original name of the file
			$sFilePath = $tempFilePathInfo->dirname . '/' . $uploadedFile['name'];
			$filePathInfo = (object) pathinfo($sFilePath);

			$counter = 1;

			// If the temporary file already exists then create a unique filename by adding a numeric suffix
			while(file_exists($sFilePath)){
				$sFilePath = $tempFilePathInfo->dirname . '/' . $filePathInfo->filename . '~' . $counter . '.' . $filePathInfo->extension;
				$counter ++;
			}

			if(move_uploaded_file($sTempFilePath, $sFilePath)){
				$result = $this->setUploadFromFile($sField, $sFilePath, $sSaveToFolder, $sComments, $iTimestamp);

				return $result;
			}
			else{
				throw new Exception('The uploaded file could not be renamed');
			}
		}
		else{
			throw new Exception('The file is not an uploaded file');
		}
	}

	public function removeUpload($sField, $bDeleteFile = true){
		if($upload = $this->getUpload($sField)){
			if($bDeleteFile){
				if(file_exists($upload->abspath)){
					if(unlink($upload->abspath)){
						// OK
					}
					else{
						throw new Exception('The file "' . $upload->name . '" could not be deleted');
					}
				}
			}
		}

		$aUploadItems = array();
		$this->{$sField} = json_encode($aUploadItems);

		return true;
	}

	public function removeMultiUpload($sField, $bDeleteFiles = true){
		if($aUploads = $this->getMultiUpload($sField)){
			foreach($aUploads as $upload){
				if($bDeleteFiles){
					if(file_exists($upload->abspath)){
						if(@unlink($upload->abspath)){
							// OK
						}
						else{
							throw new Exception('The file "' . $upload->webpath . "' could not be deleted");
						}
					}
				}
			}
		}

		$aUploadItems = array();
		$this->{$sField} = json_encode($aUploadItems);

		return true;
	}
}