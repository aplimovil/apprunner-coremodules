<?php
trait Mapping {
	function __get($name){
		if($property = static::$mapping[$name]){
			return $this->{$property};
		}
		else throw new Exception(_T('The property "%s" is not defined in the mapping or property list of the class "%s".', $name, get_class($this)));
	}

	function __set($name, $value){
		if($property = static::$mapping[$name]){
			$this->{$property} = $value;
		}
		else throw new Exception(_T('The property "%s" is not defined in the mapping or property list of the class "%s".', $name, get_class($this)));
	}

	function __isset($name){
		return static::$mapping[$name] ? true : false;
	}

	static function getMappedProperty($name){
		if($property = static::$mapping[$name]){
			return $property;
		}
		else return false;
	}

	static function getReverseMappedProperty($name){
		if($reverseProperty = array_search($name, static::$mapping)){
			return $reverseProperty;
		}
		else return false;
	}

	static function getMappedArray($array){
		$newArray = array();

		foreach($array as $key => $value){
			$mappedKey = static::getMappedProperty($key);
			if(!$mappedKey) $mappedKey = $key;
			
			$newArray[$mappedKey] = $value;
		}

		return $newArray;
	}

	static function findAll($where = null, $orderBy = false, $limit = 0, $offset = 0, $having = null, $additionalColumns = null){
		if(is_array($where)){
			$where = static::getMappedArray($where);
		}

		if(Util::classHasTrait(get_called_class(), 'SoftDeletes')){
			return static::findAllWithStatusFilter($where, $orderBy, $limit, $offset, $having, $additionalColumns);
		}
		else{
			return parent::findAll($where, $orderBy, $limit, $offset, $having, $additionalColumns);
		}
	}

	static function findFirst($where = null, $orderBy = false, $having = null, $additionalColumns = null){
		if(is_array($where)){
			$where = static::getMappedArray($where);
		}

		if(Util::classHasTrait(get_called_class(), 'SoftDeletes')){
			return static::findFirstWithStatusFilter($where, $orderBy, $having, $additionalColumns);
		}
		else{
			return parent::findFirst($where, $orderBy, $having, $additionalColumns);
		}
	}
}