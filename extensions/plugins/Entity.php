<?php
trait Entity {
	/* Do not remove these properties to prevent errors when using the Validation trait */
	public $id;
	public $name;

	/*
	// Add the following optional static variables to your model:
	static $route = ''; // Route to this model's catalog, used by getCatalogLink() and getRouteLink(). You can insert '{id}' inside the route to indicate a parent record ID in the route, to be used by getRouteLink().
	static $filterRoute = true; // Make getCatalogLink() return a filtered view of the route only displaying the given record. Set to false if you're using allowSearch=false in the model's catalog configuration.
	*/

	public function getRoute(){
		$class = get_class($this);
		
		if(property_exists($class, 'route')){
			return $class::$route;
		}
		else throw new Exception('The "entity" trait requires a "route" static property to be set in the class definition of the "' . $class . '" model.');
	}

	public function allowFilterRoute(){
		if(property_exists($this, 'filterRoute')){
			$class = get_class($this);
			return $class::$filterRoute;
		}
		else return true;
	}

	public function getPath($append = ''){
		global $APP;
		
		$route = $this->getRoute();

		return $APP->translatePath($route . ($append != '' ? ('/' . $append) : ''));
	}

	public function getLabel(){
		$class = get_class($this);

		if(property_exists($class, 'label')){
			return $class::$label;
		}
		else throw new Exception('The "entity" trait requires a "label" static property to be set in the class definition of the "' . $class . '" model.');
	}

	public function name(){
		global $APP;

		if(isset($this->{$APP->settings->NAME_FIELD})){
			return $this->{$APP->settings->NAME_FIELD};
		}
		elseif(property_exists(get_called_class(), 'label') && static::$label != ''){
			return static::$label . ' #' . $this->getId();
		}
		else{
			return get_class($this) . ' #' . $this->getId();
		}
	}

	public function getName(){ // Alias of name()
		return $this->name();
	}

	public function getLink($recordId = null, $isEditMode = false, $route = '', $permalink = false){
		return $this->getCatalogLink($recordId, $isEditMode, $route, $permalink);
	}

	public function getCatalogLink($recordId = null, $isEditMode = false, $route = '', $permalink = false){
		global $APP;

		if($recordId == null){
			if($this instanceof Model){
				$recordId = $this->getId();
			}
		}

		$modelRoute = $this->getRoute();
		$path = $permalink ? $APP->url($route ? $route : $modelRoute) : $APP->path($route ? $route : $modelRoute);

		if($this->allowFilterRoute()){
			if($recordId) $path .= "?__catalog_action=catalog-search&f_id={$recordId}";
			if($isEditMode) $path .= "#{$recordId}";
		}

		return $path;
	}
	
	public function getRouteLink($iParentRecordID, $recordId = 0, $route = '', $permalink = false){
		global $APP;
		
		if($recordId === 0){
			if($this instanceof Model){
				$recordId = $this->getId();
			}
		}

		$modelRoute = $this->getRoute();
		$path = $permalink ? $APP->url($route ? $route : $modelRoute) : $APP->path($route ? $route : $modelRoute);

		if(strpos($path, '{id}') !== false){ // If there's an {id} placeholder replace it with parent record ID
			$path = str_replace('{id}', $iParentRecordID, $path);
		}
		else{ // Otherwise then append it to the path
			$path .=  '/' . $iParentRecordID;
		}

		if($recordId){
			$path .=  "?__catalog_action=catalog-search&f_id={$recordId}";
		}

		return $path;
	}

	public function getSearchLink($searchFields, $route = '', $permalink = false){
		global $APP;
		
		$aSearchTerms = [];
		
		foreach($searchFields as $sField => $sValue){
			$sValue = rawurlencode($sValue);

			// Decode curly brackets to use them as field placeholders (ej. {id})
			$sValue = str_replace('%7B', '{', $sValue);
			$sValue = str_replace('%7D', '}', $sValue);

			$aSearchTerms[] = "f_{$sField}=" . $sValue;
		}

		$modelRoute = $this->getRoute();
		$path = ($permalink ? $APP->url($route ? $route : $modelRoute) : $APP->path($route ? $route : $modelRoute)) . "?__catalog_action=catalog-search&" . join("&", $aSearchTerms);

		return $path;
	}
}