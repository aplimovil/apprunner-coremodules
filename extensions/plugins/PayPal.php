<?php
/*
- EXAMPLE: -
PayPal::configure([
	'ACCOUNT' => 'paypal@domain.com',
	'CURRENCY' => 'USD'
]);

// Make a one-time payment (Buy Now)
PayPal::charge([
	'item' => _T('PRODUCT NAME'),
	'amount' => 10,
	'custom' => '',
	'return_url' => '',
	'cancel_url' => ''
], PAYPAL::BUY_NOW);

// Make a subscription
PayPal::charge([
	'item' => _T('SUBSCRIPTION NAME'),
	'amount' => 10,
	'interval' => 'M' // Options: D=Days, W=Weeks, M=Months, Y=Years
	'duration' => 1, // duration & interval = recurrence (e.g. 7 days, 2 weeks, 1 month, 1 year)
	'custom' => '',
	'return_url' => '',
	'cancel_url' => ''
], PAYPAL::SUBSCRIPTION);
*/
class PayPal {
	const BUY_NOW = 'buynow';
	const SUBSCRIPTION = 'subscription';

	public static $live = true;
	public static $log = false;
	
	private static $_settings = array();
	
	static function configure($aSettings){
		self::$_settings = (object) $aSettings;
		if(!isset(self::$_settings->redirect_delay)){
			self::$_settings->redirect_delay = 0;
		}
		if(self::$_settings->SANDBOX){
			self::$live = false;
		}
		if(self::$_settings->LOG){
			self::$log = true;
		}
	}
	
	static function charge($aOptions, $type = self::BUY_NOW){
		$charge = (object) $aOptions;
		
		$sPaymentURL = "https://www.paypal.com/cgi-bin/webscr";
		
		if(self::$_settings->SANDBOX){
			$sPaymentURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		}
?>
<form name="PayPal" action="<?= $sPaymentURL ?>" method="post">
  <? if($type == self::BUY_NOW): ?>
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="item_name" value="<?= $charge->item ?>">
	<input type="hidden" name="amount" value="<?= $charge->amount ?>">
  <? elseif($type == self::SUBSCRIPTION): ?>
	<input type="hidden" name="cmd" value="_xclick-subscriptions">
	<input type="hidden" name="item_name" value="<?= $charge->item ?>">
	<input type="hidden" name="a3" value="<?= $charge->amount ?>">
	<input type="hidden" name="p3" value="<?= $charge->duration ?: 1 ?>">
	<input type="hidden" name="t3" value="<?= $charge->interval ? strtoupper($charge->interval) : 'M' ?>">
	<input type="hidden" name="src" value="<?= $charge->recur ?: 1 ?>">
	<input type="hidden" name="no_note" value="0">
  <? endif; ?>

	<input type="hidden" name="business" value="<?= self::$_settings->ACCOUNT ?>">
	<input type="hidden" name="currency_code" value="<?= self::$_settings->CURRENCY ?>">

  <? if($charge->allowCustomerNote): ?>
	<input type="hidden" name="no_note" value="1">
  <? endif; ?>

  <? if(!isset($charge->shipping) || $charge->shipping === false): ?>
	<input type="hidden" name="no_shipping" value="1">
  <? else: ?>
	<input type="hidden" name="shipping" value="<?= $charge->shipping ?>">
  <? endif; ?>

  <? if(is_array($charge->customer)): ?>
	<input type="hidden" name="first_name" value="<?= $charge->customer['firstName'] ?>">
	<input type="hidden" name="last_name" value="<?= $charge->customer['lastName'] ?>">
	<input type="hidden" name="address1" value="<?= $charge->customer['address1'] ?>">
	<input type="hidden" name="address2" value="<?= $charge->customer['address2'] ?>">
	<input type="hidden" name="city" value="<?= $charge->customer['city'] ?>">
	<input type="hidden" name="state" value="<?= $charge->customer['state'] ?>">
	<input type="hidden" name="zip" value="<?= $charge->customer['postalCode'] ?>">
	<input type="hidden" name="country" value="<?= $charge->customer['country'] ?>">
	<input type="hidden" name="night_phone_b" value="<?= $charge->customer['phone'] ?>">
  <? endif; ?>

	<input type="hidden" name="rm" value="1">
	<input type="hidden" name="return" value="<?= $charge->return_url ?>">
	<input type="hidden" name="cancel_return" value="<?= $charge->cancel_url ?>">
	<input type="hidden" name="weight_unit" value="lbs">
	<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
	<input type="hidden" name="lc" value="<?= self::$_settings->COUNTRY ? self::$_settings->COUNTRY : 'en' ?>">
	<input type="hidden" name="locale.x" value="<?= self::$_settings->LOCALE ? self::$_settings->LOCALE : 'en_US' ?>">
	<input type="hidden" name="page_style" value="<?= $charge->page_style ?>" />
	<input type="hidden" name="custom" value="<?= urlencode($charge->custom) ?>">
	<noscript>
	<input type="submit" name="submit" value="Click here to proceed to payment &raquo;" />
	</noscript>
</form>

</form>
<script type="text/javascript">
window.onload = function(){
	setTimeout(function(){
		document.forms.PayPal.submit();
	}, <?= (self::$_settings->redirect_delay * 1000) ?>);
};
</script>
<?
	}
	
	static function processIPN($callback = null){		
		ob_start();
		
		$sRequest = 'cmd=_notify-validate';
		
		foreach ($_POST as $sKey => $sValue) {
			$sValue = urlencode(stripslashes($sValue));
			$sRequest .= "&$sKey=$sValue";
		}
		
		$sHeader  = "POST /cgi-bin/webscr HTTP/1.1\r\n";
		$sHeader .= "Host: www.sanbox.paypal.com\r\n";
		$sHeader .= "Accept: */*\r\n";
		$sHeader .= "Connection: Close\r\n";
		$sHeader .= "Content-Length: " . strlen($sRequest) . "\r\n";
		$sHeader .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$sHeader .= "\r\n";
		
		if(self::$live){
			$sURL = "ssl://www.paypal.com";
		}
		else{
			$sURL = "ssl://www.sandbox.paypal.com";
		}
		
		$objConn = fsockopen($sURL, 443, $errno, $errstr, 30);
		
		echo "DATA:\r\n" . join(explode("&", $sHeader . $sRequest), "\r\n") . "\r\n\r\n";
		
		$response = new stdClass;
		$response->status = 0;
		$response->error = "";
		
		if($objConn) {
			fputs($objConn, $sHeader . $sRequest);
			
			$sResponse = stream_get_contents($objConn, 2048);
			$tokens = explode("\r\n\r\n", trim($sResponse));
			$sResult = trim(end($tokens));
			
			if(strpos($sResult, "VERIFIED") !== false){
				echo ">> COMPLETED: AUTHENTICATION\r\n";
				
				if(in_array($_POST['payment_status'], array('Completed', 'Pending')) && in_array($_POST['txn_type'], array("web-accept", "web_accept", "subscr_payment")) && $_POST['receiver_email'] == self::$_settings->ACCOUNT){
					echo ">> COMPLETED: VERIFICATION\r\n";
					
					$transaction = (object) array(
						'id' => $_POST['txn_id'],
						'amount' => $_POST['mc_gross'],
						'custom' => urldecode($_POST['transaction_subject'])
					);
					
					$response->status = 1;
					$response->transaction = $transaction;
				}
				else{
					echo ">> ERROR: TRANSACTION IS NOT VALID\r\n";
					$response->error = "La transaccion no es valida.";
				}
			}
			else{
				echo ">> TRANSACTION RESULT: {$sResult}\r\n";
				echo ">> ERROR: TRANSACTION COULD NOT BE VERIFIED\r\n";
				$response->error = "La transaccion no pudo ser verificada.";
			}
		}
		else{
			echo ">> ERROR: CONNECTION FAILED\r\n";
			$response->error = "Fallo la conexion a PayPal";
		}
		
		fclose($objConn);
		
		if($callback){
			$callback($response);
		}
		
		if(self::$log){
			$Log = fopen("paypal-log.txt", "a");
			fwrite($Log, ob_get_contents());
			fwrite($Log, "\r\n----------\r\n\r\n");
			fclose($Log);
		}
		
		ob_clean();
		
		return $response;
	}
}