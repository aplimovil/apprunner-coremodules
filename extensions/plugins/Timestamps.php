<?
trait Timestamps {
	function getCurrentTime(){
		date_default_timezone_set("GMT");
		return time();
	}

	function updateTimestamp(){
		$sTable = $this->getTableName();

		// Update the timestamp with a direct query as doing it using save() would trigger the onSave() event handler and create an infinite loop
		DB::Query("UPDATE `{$sTable}` SET timestamp = " . $this->getCurrentTime() . " WHERE id = {$this->id}");
	}
	
	function onSave(){
		$this->updateTimestamp();
	}

	function onDelete(){
		$this->updateTimestamp();
	}
}
?>