<?php
/*
@name			Persistent Monitored Model (PDBM)
@description	Monitored version of the Persistent Model class, that allows recording all inserts, updates, and deletes in a user activity log
*/

// DEPRECATED, use the "SoftDeletes" and "Audit" traits instead in the model class instead
abstract class PersistentMonitoredModel extends Model {
	use SoftDeletes, Audit;
}