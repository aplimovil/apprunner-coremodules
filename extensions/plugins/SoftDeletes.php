<?php
trait SoftDeletes {
	public static $deletedField = 'status';
	public static $deletedValue = 0;
	public static $activeValue = 1;
	
	public static function findAll($where = null, $orderBy = false, $limit = 0, $offset = 0, $having = null, $additionalColumns = null){
		return static::findAllWithStatusFilter($where, $orderBy, $limit, $offset, $having, $additionalColumns);
	}

	public static function findAllWithStatusFilter($where = null, $orderBy = false, $limit = 0, $offset = 0, $having = null, $additionalColumns = null){
		$class = get_called_class();

		if(!$orderBy && property_exists($class, 'order')) $orderBy = static::$order;

		$where = static::appendStatusFilter($where);
		
		return DB::FindAll($class, $where, $orderBy, $limit, $offset, $having, $additionalColumns);
	}

	public static function findFirst($where = null, $orderBy = false, $having = null, $additionalColumns = null){
		return static::findFirstWithStatusFilter($where, $orderBy, $having, $additionalColumns);
	}

	public static function findFirstWithStatusFilter($where = null, $orderBy = false, $having = null, $additionalColumns = null){
		$class = get_called_class();
		
		$where = static::appendStatusFilter($where);
		
		return DB::FindFirst($class, $where, $orderBy, $having, $additionalColumns);
	}

	public static function count($where = null, $having = null, $additionalColumns = null){
		return DB::Count(get_called_class(), static::getStatusFilter($where), $having, $additionalColumns);
	}
	
	public static function getStatusFilter($additionalFilters = null){
		$table = static::getTableName();
		$criteria = '`' . $table . '`.`' . static::$deletedField . '` != ' . DB::Escape(static::$deletedValue);

		if($additionalFilters){
			$criteria .= ' AND ' . Util::arrayToSQLFilter($additionalFilters);
		}

		return $criteria;
	}

	public static function appendStatusFilter($where){
		return DB::AppendCriteria($where, self::getStatusFilter());
	}

	public function save($excludedFields = array(), $forceInsert = false, $resetCatalogCache = true){
		if(!isset($this->{static::$deletedField})){
			$this->{static::$deletedField} = static::$activeValue;
		}
		
		return parent::save($excludedFields, $forceInsert, $resetCatalogCache);
	}
	
	public function delete(){
		$this->checkConstraints();

		$class = get_class($this);
		$table = DB::Class2Table($class);
		$primaryKey = DB::EscapeField($class::getPrimaryKeyName());
		$id = DB::EscapeValue($this->getId());

		$beforeDeleteResult = $this->onBeforeDelete();
		if($beforeDeleteResult === false) return false;

		DB::Query("UPDATE `{$table}` SET " . static::$deletedField . " = 0 WHERE {$primaryKey} = {$id}");
		$affectedRows = DB::AffectedRows();

		$this->_onDelete();
		$this->onDelete();
		$this->onAfterDelete();

		$this->resetCatalogCache();

		return ($affectedRows != 0 ? true : false);

	}
}