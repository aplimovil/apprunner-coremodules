<?php
/*
@name			Monitored Model (DBM)
@description	Monitored version of the Model class, that allows recording all inserts, updates, and deletes in a user activity log
*/

// DEPRECATED, use the "Monitor" trait instead
abstract class MonitoredModel extends Model {
	use Monitor;
}