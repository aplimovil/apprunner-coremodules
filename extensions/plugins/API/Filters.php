<?php
// This file is located here instead of the API module as the latter is only loaded in /api/ routes
namespace API;

interface Filters {
	public static function API_Before(&$params);
	public static function API_Query($params, &$columns, &$from, &$where, &$having, &$order, &$limit);
	public static function API_Item(&$record, $params);
	public static function API_Results(&$results, $params);
	//public static function API_Response(&$response, $params);
}