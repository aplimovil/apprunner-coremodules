<?php
// This file is located here instead of the API module as the latter is only loaded in /api/ routes

namespace API;

interface Events {
	public static function onAPICreate(&$entity, $params, $isBefore);
	public static function onAPIUpdate(&$entity, $params, $isBefore);
	public static function onAPIDelete(&$entity, $params, $isBefore);
}