<?php
trait ValidationWithAudit {
	use Validation, Audit {
		Validation::_onBeforeSave insteadof Audit;
	}

	function onBeforeSave(){
		Audit::_onBeforeSave($this);
	}
}