<?php
abstract class CatalogController extends Controller {
	protected $catalog;

	function init($catalogTitle, $modelClass, $modelName = null){
		if(!$modelName) $modelName = $modelClass;

		$this->setTitle($catalogTitle);

		$this->catalog = new EntityCatalog($modelClass, $modelName);

		return $this->catalog;
	}

	public final function getCatalog(){
		return $this->catalog;
	}

	public function view(){
		$this->catalog->view();
	}
}