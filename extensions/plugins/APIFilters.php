<?php
// This file is located here instead of the API module as the latter is only loaded in /api/ routes
interface APIFilters extends API\Filters {}