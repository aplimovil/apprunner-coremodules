<?php
/*
NOTES:
- Requires the "Uploads" trait. Supports both single-upload and multi-upload fields (by putting the upload index within brackets after the field name).
- The runImageResizer() method must be called once (right after the upload is received) in order to generate the resized versions of the image and save them to disk
*/

trait ImageResizer {
	static $_placeholderPaths = [
		'large' => 'modules/core/extensions/interface/images/ImageResizer/placeholder-large.jpg',
		'medium' => 'modules/core/extensions/interface/images/ImageResizer/placeholder-medium.jpg',
		'small' => 'modules/core/extensions/interface/images/ImageResizer/placeholder-small.jpg'
	];

	/*
	Usage for $createLargeImage, $createMediumImage and $createSmallImage static properties:
	----
	static $createLargeImage = false;
	
	- OR -
	
	static $createLargeImage = [
		'IMAGE UPLOAD FIELD' => false
	];
	*/

	static $_createLargeImage = true;
	static $_createMediumImage = true;
	static $_createSmallImage = true;

	static $_largeImageSize = [1200, 902];
	static $_mediumImageSize = [700, 527];
	static $_smallImageSize = [400, 300];

	static $_resizesFolderPath = 'data/uploads/resizes';
	static $_resizeQuality = 95;

	static $_removeImagesOnDelete = true;

	function _onDelete($model = null){
		if(isset(static::$removeImagesOnDelete)){
			static::$removeImagesOnDelete = static::$_removeImagesOnDelete;
		}

		if(static::$_removeImagesOnDelete){
			if(!$model) $model = $this;

			foreach($model as $field => $value){
				if($model->isUploadField($field)){
					$model->deleteResizes($field);
				}
			}
		}
	}

	/*** Original image ***/

	// Supports retrieval of a particular upload index from a multi-upload field by adding the index within brackets as a suffix to the upload field name
	public function getImage($field){
		return $this->getUpload($field);
	}
	
	public function getImageFilePath($field){
		if($image = $this->getImage($field)){
			return $image->path;
		}
		else return '';
	}

	public function getImagePath($field, $absolute = true){
		if($image = $this->getImage($field)){
			return $absolute ? $image->abspath : (APP::FOLDER_UPLOADS . '/' . $image->path);
		}
		else return '';
	}

	public function getImageURL($field, $absolute = true){
		return $this->getUploadUrl($field, $absolute);
	}

	/*** Large image ***/

	private function getLargeImageFolder($field){
		$path = isset(static::$resizesFolderPath) ? static::$resizesFolderPath : static::$_resizesFolderPath;
		return $path . '/' . get_class($this) . '-' . $this->prepareFieldName($field) . '/large';
	}

	public function getLargeImageFile($field){
		if($image = $this->getImage($field)){
			return $this->getLargeImageFolder($field) . '/' . $image->name;
		}
		else return '';
	}

	public function getLargeImagePath($field, $absolute = true, $cropDimensions = null, $cropPosition = 'center'){
		global $APP;

		$filePath = $this->getLargeImageFile($field);

		if($cropDimensions){
			$filePath = $this->getProcessedImagePath($field, $filePath, $cropDimensions, $cropPosition);
		}

		return $filePath ? $APP->translatePath($filePath, $absolute) : false;
	}

	public function getLargeImageURL($field, $absolute = true, $cropDimensions = null, $cropPosition = 'center'){
		return $this->getProcessedImageURL($field, $this->getLargeImageFile($field), $absolute, 'large', $cropDimensions, $cropPosition);
	}

	public function getLargeImageWidth(){
		$size = (isset(static::$largeImageSize) ? static::$largeImageSize : static::$_largeImageSize);
		return $size[0];
	}

	public function getLargeImageHeight(){
		$size = (isset(static::$largeImageSize) ? static::$largeImageSize : static::$_largeImageSize);
		return $size[1];
	}

	/*** Medium image ***/

	private function getMediumImageFolder($field){
		$path = isset(static::$resizesFolderPath) ? static::$resizesFolderPath : static::$_resizesFolderPath;
		return $path . '/' . get_class($this) . '-' . $this->prepareFieldName($field) . '/medium';
	}

	public function getMediumImageFile($field){
		if($image = $this->getImage($field)){
			return $this->getMediumImageFolder($field) . '/' . $image->name;
		}
		else return '';
	}

	public function getMediumImagePath($field, $absolute = true, $cropDimensions = null, $cropPosition = 'center'){
		global $APP;

		$filePath = $this->getMediumImageFile($field);

		if($cropDimensions){
			$filePath = $this->getProcessedImagePath($field, $filePath, $cropDimensions, $cropPosition);
		}

		return $filePath ? $APP->translatePath($filePath, $absolute) : false;
	}

	public function getMediumImageURL($field, $absolute = true, $cropDimensions = null, $cropPosition = 'center'){
		return $this->getProcessedImageURL($field, $this->getMediumImageFile($field), $absolute, 'medium', $cropDimensions, $cropPosition);
	}

	public function getMediumImageWidth(){
		$size = (isset(static::$mediumImageSize) ? static::$mediumImageSize : static::$_mediumImageSize);
		return $size[0];
	}

	public function getMediumImageHeight(){
		$size = (isset(static::$mediumImageSize) ? static::$mediumImageSize : static::$_mediumImageSize);
		return $size[1];
	}

	/*** Small image ***/

	private function getSmallImageFolder($field){
		$path = isset(static::$resizesFolderPath) ? static::$resizesFolderPath : static::$_resizesFolderPath;
		return $path . '/' . get_class($this) . '-' . $this->prepareFieldName($field) . '/small';
	}

	public function getSmallImageFile($field){
		if($image = $this->getImage($field)){
			return $this->getSmallImageFolder($field) . '/' . $image->name;
		}
		else return '';
	}

	public function getSmallImagePath($field, $absolute = true, $cropDimensions = null, $cropPosition = 'center'){
		global $APP;
		
		$filePath = $this->getSmallImageFile($field);

		if($cropDimensions){
			$filePath = $this->getProcessedImagePath($field, $filePath, $cropDimensions, $cropPosition);
		}

		return $filePath ? $APP->translatePath($filePath, $absolute) : false;
	}

	public function getSmallImageURL($field, $absolute = true, $cropDimensions = null, $cropPosition = 'center'){
		return $this->getProcessedImageURL($field, $this->getSmallImageFile($field), $absolute, 'small', $cropDimensions, $cropPosition);
	}

	public function getSmallImageWidth(){
		$size = (isset(static::$smallImageSize) ? static::$smallImageSize : static::$_smallImageSize);
		return $size[0];
	}

	public function getSmallImageHeight(){
		$size = (isset(static::$smallImageSize) ? static::$smallImageSize : static::$_smallImageSize);
		return $size[1];
	}

	/*** General methods ***/

	/* Useful for knowing if the resized versions for an image upload field have been generated already. If it returns false, runImageResizer() should be called. */
	public function isResized($field){
		global $APP;

		$filePath = $this->getLargeImageFile($field);

		if($filePath && file_exists($APP->translatePath($filePath, true))){
			return true;
		}
		else{
			$filePath = $this->getMediumImageFile($field);

			if($filePath && file_exists($APP->translatePath($filePath, true))){
				return true;
			}
			else{
				$filePath = $this->getSmallImageFile($field);

				return ($filePath && file_exists($APP->translatePath($filePath, true)));
			}
		}
	}

	public function getThumbnailURL($field, $absolute = true){
		if($this->shouldCreateSmallImage($field)){
			return $this->getSmallImageURL($field, $absolute);
		} elseif($this->shouldCreateMediumImage($field)){
			return $this->getMediumImageURL($field, $absolute);
		} elseif($this->shouldCreateLargeImage($field)){
			return $this->getLargeImageURL($field, $absolute);
		} else {
			return $this->getImageURL($field, $absolute);
		}
	}

	public function shouldCreateLargeImage($field){
		$createLargeImage = (isset(static::$createLargeImage) ? static::$createLargeImage : static::$_createLargeImage);

		if(is_array($createLargeImage)){
			$createLargeImage = ($createLargeImage[$field] !== false && $createLargeImage[$field] !== 0);
		}

		return $createLargeImage;
	}

	public function shouldCreateMediumImage($field){
		$createMediumImage = (isset(static::$createMediumImage) ? static::$createMediumImage : static::$_createMediumImage);

		if(is_array($createMediumImage)){
			$createMediumImage = ($createMediumImage[$field] !== false && $createMediumImage[$field] !== 0);
		}

		return $createMediumImage;
	}

	public function shouldCreateSmallImage($field){
		$createSmallImage = (isset(static::$createSmallImage) ? static::$createSmallImage : static::$_createSmallImage);

		if(is_array($createSmallImage)){
			$createSmallImage = ($createSmallImage[$field] !== false && $createSmallImage[$field] !== 0);
		}

		return $createSmallImage;
	}

	/* Generates the resized versions for an image upload field on the individual entity (record) represented by the class instance */
	public function runImageResizer($field){
		$warningsEnabled = ErrorLogger::warningsEnabled();
		if($warningsEnabled) ErrorLogger::disableWarnings();

		if($image = $this->getImage($field)){
			$largeImageFolderPath = $this->getLargeImageFolder($field);
			$mediumImageFolderPath = $this->getMediumImageFolder($field);
			$smallImageFolderPath = $this->getSmallImageFolder($field);

			$smallImagePath = $this->getSmallImagePath($field, true);
			$mediumImagePath = $this->getMediumImagePath($field, true);
			$largeImagePath = $this->getLargeImagePath($field, true);

			$smallImageResizeResult = null;
			$mediumImageResizeResult = null;
			$largeImageSizeResult = null;

			if($this->shouldCreateLargeImage($field) && $largeImagePath){
				if(!is_dir($largeImageFolderPath)) mkdir($largeImageFolderPath, 0755, true);

				$largeImageResizeResult = Util::resizeImage($image->abspath, $largeImagePath, $this->getLargeImageWidth(), $this->getLargeImageHeight(), static::getResizeQuality());
			}

			if($this->shouldCreateMediumImage($field) && $mediumImagePath){
				if(!is_dir($mediumImageFolderPath)) mkdir($mediumImageFolderPath, 0755, true);

				$mediumImageResizeResult = Util::resizeImage($image->abspath, $mediumImagePath, $this->getMediumImageWidth(), $this->getMediumImageHeight(), static::getResizeQuality());
			}

			if($this->shouldCreateSmallImage($field) && $smallImagePath){
				if(!is_dir($smallImageFolderPath)) mkdir($smallImageFolderPath, 0755, true);

				$smallImageResizeResult = Util::resizeImage($image->abspath, $smallImagePath, $this->getSmallImageWidth(), $this->getSmallImageHeight(), static::getResizeQuality());
			}

			$allResult = (is_null($largeImageSizeResult) || $largeImageSizeResult) && (is_null($mediumImageResizeResult) || $mediumImageResizeResult) && (is_null($smallImageResizeResult) || $smallImageResizeResult);

			if($warningsEnabled) ErrorLogger::enableWarnings();

			return (object) [
				'all' => $allResult,
				'large' => $largeImageResizeResult,
				'medium' => $mediumImageResizeResult,
				'small' => $smallImageResizeResult
			];
		}
		else return false;
	}

	public function deleteResizes($field){
		$largeImagePath = $this->getLargeImagePath($field, true);
		$mediumImagePath = $this->getMediumImagePath($field, true);
		$smallImagePath = $this->getSmallImagePath($field, true);

		$success = true;

		if($smallImagePath && file_exists($smallImagePath)){
			if(!unlink($smallImagePath)) $success = false;
		}

		if($mediumImagePath && file_exists($mediumImagePath)){
			if(!unlink($mediumImagePath)) $success = false;
		}

		if($largeImagePath && file_exists($largeImagePath)){
			if(!unlink($largeImagePath)) $success = false;
		}

		return $success;
	}

	public function getPlaceholderURL($size, $absolute = true){
		global $APP;

		$paths = isset(static::$placeholderPaths) ? static::$placeholderPaths : static::$_placeholderPaths;

		if($placeholderPath = $paths[$size]){
			if(strpos($placeholderPath, 'http') !== 0){
				return Util::encodeURI($APP->translateUrl($placeholderPath, $absolute));
			}
			else{
				return $placeholderPath;
			}
		}
		else throw new Exception(_T('The placeholder size "%s" is not configured in the "%s" class.', $size, get_class($this)));
	}

	public function getResizeQuality(){
		return isset(static::$resizeQuality) ? static::$resizeQuality : static::$_resizeQuality;
	}

	public function getImageHTML($fieldName, $includeLink = true, $width = '100%', $height = 'auto'){
		if(substr($width, -1) != '%') $width .= 'px';
		if(substr($height, -1) != '%') $height .= 'px';

		$imageTemplate = '<img src="%s" style="width: %s; height: %s;" />';
		$linkTemplate = '<a href="%s" target="_blank">%s</a>';

		if($this->{$fieldName} && $this->getUpload($fieldName)){
			if($this->isResized($fieldName)){
				$imageUrl = $this->getImageURL($fieldName) . '?' . uniqid();
				$smallImageUrl = $this->getThumbnailURL($fieldName) . '?' . uniqid();
				$imageHtml = sprintf($imageTemplate, $smallImageUrl, $width, $height);
			}
			else{
				$imageUrl = $this->getImageURL($fieldName);
				$imageHtml = sprintf($imageTemplate, $imageUrl, $width, $height);
			}
		}
		else {
			$imageUrl = $this->getPlaceholderURL('small');
			$imageHtml = sprintf($imageTemplate, $imageUrl, $width, $height);
			$includeLink = false;
		}

		if($includeLink){
			return sprintf($linkTemplate, $imageUrl, $imageHtml);
		}
		else{
			return $imageHtml;
		}
	}

	public function getImagePreviewHTML($field, $width = 'auto', $height = 100, $linkToOriginal = true, $class = ''){
		$smallImageUrl = $this->getThumbnailURL($field, false);
		$isResized = $this->isResized($field);
		$html = '';

		if($isResized && $linkToOriginal){
			$html .= '<a href="' . $this->getImageURL($field) . '" class="' . $class . '" target="_blank">';
		}
		elseif($class){
			$html .= '<div class="' . $class . '">';
		}

		$html .= '<img width="' . $width . '" height="' . $height . '" src="' . $smallImageUrl . '" /></a>';

		if($isResized && $linkToOriginal){
			$html .= '</a>';
		}
		elseif($class){
			$html .= '</div>';
		}

		return $html;
	}

	/*** Private methods ***/

	private function prepareFieldName($field){
		$field = $this->removeUploadIndexFromFieldName($field);
		return $field;
	}

	/* Supports image croppping using cropDimensions (e.g. "200x300" [WIDTHxHEIGHT]) and cropPosition (options: top/middle/bottom-left/center/right, e.g. "top-center") */
	private function getProcessedImagePath($field, $filePath, $cropDimensions, $cropPosition){
		global $APP;

		// If filepath and cropping options are provided
		if($filePath && $cropDimensions && is_string($cropDimensions)){
			if(!preg_match('/^[0-9]+x[0-9]+$/', $cropDimensions)) throw new Exception(_T('Crop dimensions need to be in WxH string format.'));

			$renamed = 0;

			// Append the cropping options to the image filename (before the file extension)
			$croppedFilePath = preg_replace('/\.([a-z]+)/i', ('-' . $cropDimensions . '-' . $cropPosition . '.$1'), $filePath, 1, $renamed);

			if($renamed != 0){
				// Create the cropped image only if has not been generated yet
				if(!file_exists($croppedFilePath)){
					$imageFile = $APP->translatePath($filePath, true);
					if(!file_exists($imageFile)) return false;

					list($imageWidth, $imageHeight) = getimagesize($imageFile);
					$imageRatio = $imageWidth / $imageHeight;

					$dimensions = explode('x', $cropDimensions);
					$width = (int) $dimensions[0];
					$height = (int) $dimensions[1];

					$upscaleWidth = $width;
					$upscaleHeight = $height;

					if($imageHeight < $imageWidth){
						$upscaleHeight = $height;
						$upscaleWidth = round($upscaleHeight * $imageRatio);
					}
					else{
						$upscaleWidth = $width;
						$upscaleHeight = round($upscaleWidth / $imageRatio);
					}

					$x = 0;
					$y = 0;

					if($cropPosition == 'center'){
						$y = floor(($upscaleHeight / 2) - ($height / 2));
						$x = floor(($upscaleWidth / 2) - ($width / 2));
					}
					else{
						$position = explode('-', $cropPosition);

						if($position[0] == 'top'){
							$y = 0;
						}
						elseif($position[0] == 'middle'){
							$y = floor(($upscaleHeight / 2) - ($height / 2));
						}
						elseif($position[0] == 'bottom'){
							$y = $upscaleHeight - $height;
						}
						else throw new Exception(_T('Unsupported crop position: %s', $cropPosition));

						if($position[1] == 'left'){
							$x = 0;
						}
						elseif($position[1] == 'center'){
							$x = floor(($upscaleWidth / 2) - ($width / 2));
						}
						elseif($position[1] == 'right'){
							$x = $upscaleWidth - $width;
						}
						else throw new Exception(_T('Unsupported crop position: %s', $cropPosition));
					}

					if(Util::resizeImage($filePath, $croppedFilePath, $upscaleWidth, $upscaleHeight, static::getResizeQuality())){
						if(Util::cropImage($croppedFilePath, $croppedFilePath, $x, $y, $width, $height, static::getResizeQuality())){
							return $croppedFilePath;
						}
						else return false;
					}
					else return false;
				}
				else return $croppedFilePath;
			}
			else return false;
		}
		else{
			$resizesPath = isset(static::$resizesFolderPath) ? static::$resizesFolderPath : static::$_resizesFolderPath;

			if($filePath && strpos($filePath, $resizesPath) !== false){ // If the file requested is a resized image
				// If no cropping options were provided then return the URL resized image (if it exists), which was generated initially by calling the runImageResizer() method
				if($filePath && file_exists($APP->translatePath($filePath, true))){
					return $filePath;
				}
				else return false;
			}
			else{ // If the original file is being requested
				if($upload = $this->getUpload($field)){
					return $upload->getPath();
				}
				else return false;
			}
		}
	}

	private function getProcessedImageURL($field, $filePath, $absolute, $placeholderSize, $cropDimensions, $cropPosition){
		global $APP;

		if($path = $this->getProcessedImagePath($field, $filePath, $cropDimensions, $cropPosition)){
			return $absolute ? Util::encodeURI($APP->translateUrl($filePath)) : $APP->translatePath($filePath);
		}
		else{
			return $this->getPlaceholderURL($placeholderSize, $absolute);
		}
	}
}