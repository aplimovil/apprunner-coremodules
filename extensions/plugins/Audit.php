<?php
trait Audit {
	static $monitoredActions = array('create', 'update', 'delete'); // Override in model to specify which actions should be logged
	static $auditLogModel = 'user_activity';
	static $logAnonymous = false;

	public function _onBeforeSave($model = null){
		global $USER;

		if(!$USER && !static::$logAnonymous) return;
		
		if(!$model) $model = $this;
		
		$sTableName = $model->getTableName();
		$aPublicProperties = UTIL::getPublicProperties($model);
		$sOldData = "";		
		$newRecord = new stdClass;
		
		foreach($model as $field => $value){
			// Exclude ID field, hidden fields (protected/private properties), or empty values
			if(in_array($field, array('id')) || !in_array($field, $aPublicProperties) || $value == '') continue;

			// Hide value of hidden fields (private/protected properties)
			if(!in_array($field, $aPublicProperties)) $value = 'N/A';
			
			$newRecord->{$field} = $value;
		}
		
		if(!$model->id){
			$iActionType = USER_ACTIVITY_TYPE_INSERT;
		}
		else{
			$iActionType = USER_ACTIVITY_TYPE_UPDATE;
			
			$oldRecord = new stdClass;
			
			if($currentRecord = DB::FindById(get_class($model), $model->id)){
				foreach($currentRecord->getData() as $field => $value){
					// Exclude ID field or empty values
					if(in_array($field, array('id')) || !in_array($field, $aPublicProperties) || $value == '') continue;

					if($newRecord->{$field} != $value){
						$oldRecord->$field = $value;
					}
					else{
						unset($newRecord->{$field});
					}
				}
			}
			
			$sOldData = json_encode($oldRecord);
		}
		
		$sNewData = json_encode($newRecord);
	
		$auditLogModel = static::$auditLogModel;

		$auditLogModel::record(array(
			"user_id" => ($USER ? $USER->id : 0),
			"type" => $iActionType,
			"table" => $sTableName,
			"record_id" => $model->id,
			"old_data" => $sOldData,
			"new_data" => $sNewData
		));
	}

	public function _onDelete($model = null){
		global $USER;

		if(!$model) $model = $this;

		$auditLogModel = static::$auditLogModel;
		
		if(in_array('delete', static::$monitoredActions)){
			if(DB::AffectedRows() != 0){
				$auditLogModel::record(array(
					"user_id" => $USER->id,
					"type" => USER_ACTIVITY_TYPE_DELETE,
					"table" => $model->getTableName(),
					"record_id" => $model->id
				));
			}
		}
	}
}