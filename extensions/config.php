<?php
APP::loadModule('core/extensions/geo');
APP::loadModule('core/extensions/phpmailer');

if(!function_exists('password_hash')){
	APP::loadPlugin('libs/password-compat');
}

AutoLoader::registerAliases(array( // For backwards compatibility with AppRunner version < 3.7
	'api' => 'API',
	'entity' => 'Entity',
	'geolocation' => 'Geolocation',
	'mapping' => 'Mapping',
	'monitor' => 'Monitor',
	'synced' => 'Synced',
	'uploads' => 'Uploads',
	'validation' => 'Validation'
));