<? $this->addBufferToHeader(function(){ ?>
<script type="text/javascript">
$(function(){
	setInterval(function(){
		$.get('<?= $this->path('admin/login-keeper/ping') ?>', null, function(){
			console.log("LoginKeeper: Ping successful.");
		}, function(){
			console.error("LoginKeeper: Ping unsuccessful.");
		});
	}, <?= (LoginKeeper\PING_INTERVAL_SECONDS * 1000) ?>);
});
</script>
<? }); ?>