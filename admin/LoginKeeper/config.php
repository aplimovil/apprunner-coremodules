<?php
namespace LoginKeeper;

use APP;

const PING_INTERVAL_SECONDS = 60;

APP::onViewLoad(function(){
	$this->loadTemplate('LoginKeeper');
});