<? $this->startBuffer() ?>
<script type="text/javascript">
$(function(){
	var $confirmPasswordRow = $('#__catalog_confirm_password').parents('tr');

	$confirmPasswordRow.hide();

	$('#__catalog_new_password').keyup(function(){
		if($.trim(this.value) != ''){
			$confirmPasswordRow.css({display: ''});
		}
		else{
			$confirmPasswordRow.hide();
		}
	});

	if(window.CATALOG){
		CATALOG.onCreate(function(){
			$confirmPasswordRow.hide();
		});

		CATALOG.onUpdate(function(){
			$confirmPasswordRow.hide();
		});
	}

	FormValidator.onSubmit(function(form, e){
		var newPassword = $('#__catalog_new_password').val();
		var confirmPassword = $('#__catalog_confirm_password').val();

		if(newPassword != ''){
			if(newPassword != confirmPassword){
				alert('<?= _T('Las contraseñas no coinciden.') ?>');
				return false;
			}
		}
	});
});
</script>
<? $this->addBufferToHeader() ?>

<? $this->view() ?>