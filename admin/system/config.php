<?php
APP::onConfigLoad(function(){
	global $ADMIN_MENU;
	
	Util::extend($ADMIN_MENU, array(
		array('path' => 'admin/system', 'label' => MENU_SYSTEM, 'inactive' => true, 'items' => array(
			array('path' => 'users', 'label' => Lang::translate(MENU_SYSTEM_USERS, 'admin/system')),
			array('path' => 'activity', 'label' => Lang::translate(MENU_SYSTEM_USERACTIVITY, 'admin/system'))
		))
	));
});