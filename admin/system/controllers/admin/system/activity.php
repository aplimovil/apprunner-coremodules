<?php
class CoreAdminSystem_AdminSystemActivityController extends Controller {
	protected $catalog;

	public function main($APP){
		global $USER_ACTIVITY_TYPE;
		
		$APP->setTitle(MENU_SYSTEM_USERACTIVITY);

		$this->catalog = new EntityCatalog("user_activity", LABEL_USERACTIVITY);

		$this->catalog->configure(array(
			"allowCreate" => false,
			"allowUpdate" => false,
			"allowDelete" => false
		))
		->addFields(array(
			"date" => array(
				"label" => LABEL_USERACTIVITY_DATE,
				"type" => EntityCatalog::FIELD_DATE,
				"date_format" => "d/m/Y g:i a",
				"range_search" => true
			),
			"user_id" => array(
				"label" => LABEL_USERACTIVITY_USER,
				"catalog" => [
					'name' => 'user'
				]
			),
			"type" => array(
				"label" => LABEL_USERACTIVITY_TYPE,
				"catalog" => $USER_ACTIVITY_TYPE
			),
			"table" => array(
				"label" => LABEL_USERACTIVITY_TABLE,
				"listable" => false,
				"searchable" => true
			),
			"record_id" => array(
				"label" => LABEL_USERACTIVITY_RECORD,
				"listable" => false,
				"searchable" => true
			)
		));

		$this->catalog->getTableView()
		->paginate()
		->setOrder("date", LABEL_USERACTIVITY_DATE, "DESC")
		->addField(array(
			"name" => "table",
			"label" => LABEL_USERACTIVITY_OBJECT,
			"format" => TABLEVIEW::FORMAT_FUNCTION,
			"sortable" => true,
			"function" => function($tableName, $activity){
				global $APP;

				$recordName = "";
				$objectName = $tableName;
				$nameField = $APP->settings->NAME_FIELD;
				
				if($tableName && $activity->record_id){
					if(class_exists($tableName)){
						if($record = DB::FindById($tableName, $activity->record_id)){
							if(method_exists($record, 'name')){
								$recordName = $record->name();
							}
							elseif(method_exists($record, 'getName')){
								$recordName = $record->getName();
							}
							elseif(property_exists($record, $nameField)){
								$recordName = $record->{$nameField};
							}
						}
					}
					else{
						if($name = @DB::GetValue("SELECT {$nameField} FROM {$tableName} WHERE id = ?", [$activity->record_id])){
							$recordName = $name;
						}
					}

					$objectName .= " #{$activity->record_id}";
				}
				
				if($recordName){
					$objectName .= ": {$recordName}";
				}
				
				return $objectName;
			}
		))
		->addField(array(
			"name" => "old_data",
			"label" => LABEL_USERACTIVITY_OLDDATA,
			"format" => TABLEVIEW::FORMAT_FUNCTION,
			"function" => function($sOldData, $activity){
				if(ltrim($sOldData) != ""){
					$data = json_decode($sOldData);
					$sHTML = "";

					if(is_array($data) || is_object($data)){
						foreach($data as $field => $value){
							$sHTML .= "{$field}: {$value}<br />";
						}
					}
					
					return $sHTML;
				}
			}
		))
		->addField(array(
			"name" => "new_data",
			"label" => LABEL_USERACTIVITY_NEWDATA,
			"format" => TABLEVIEW::FORMAT_FUNCTION,
			"function" => function($sNewData, $activity){
				if(ltrim($sNewData) != ""){
					$data = json_decode($sNewData);
					$sHTML = "";

					if(is_array($data) || is_object($data)){
						foreach($data as $field => $value){
							$sHTML .= "{$field}: {$value}<br />";
						}
					}
					return $sHTML;
				}
			}
		));
	}

	public function viewCatalog(){
		$this->catalog->view();
	}
}