<?php
class CoreAdminSystem_AdminSystemUsersController extends CatalogController {
	public function main($APP){
		global $USER_TYPE, $USER_STATUS;

		$this
		->init(Lang::translate(MENU_SYSTEM_USERS, 'admin/system'), 'user', Lang::translate(LABEL_USER, 'admin/system'))
		->configure(array(
			'width' => '100%',
			'dialogWidth' => 600,
			'dialogHeight' => 350
		))
		->onBeforeCreate(function(&$user, $catalog){
			global $APP;

			if(ltrim($_POST['new_password']) == ''){
				$APP->error(ERROR_PASSWORD_MISSING);
				return false;
			}

			return $catalog->runOnBeforeUpdate($user);
		})
		->onBeforeUpdate(function(&$user, $catalog){
			global $APP;

			if(array_pop(DB::Query("SELECT COUNT(*) FROM user WHERE username = '{$user->username}' AND id != '{$user->id}' AND status != 0")->fetch_array()) != 0){
				$APP->error(ERROR_DUPLICATE_USER);
				return false;
			}

			$newPassword = trim($_POST['new_password']);

			if($newPassword != ''){
				$confirmedPassword = $_POST['confirm_password'];
				if($newPassword === $confirmedPassword){
					$user->setHash($newPassword);
				}
				else{
					$APP->error(ERROR_USER_PASSWORD_CONFIRMATION_MISMATCH);
					return false;
				}
			}
		})
		->where("username != 'admin'")
		->orderBy('name', LABEL_USER_FULL_NAME, 'ASC')
		->addFields(array(
			'username' => array(
				'label' => Lang::translate(LABEL_USER_USERNAME, 'admin/system'),
				'required' => true,
				'listable' => true
			),
			'new_password' => array(
				'label' => LABEL_USER_NEW_PASSWORD,
				'type' => 'password',
				'ignore' => true,
				'listable' => false,
				'searchable' => false,
				'comments' => LABEL_USER_NEW_PASSWORD_FIELD_COMMENTS
			),
			'confirm_password' => array(
				'label' => LABEL_USER_CONFIRM_PASSWORD,
				'type' => 'password',
				'ignore' => true,
				'listable' => false,
				'searchable' => false
			),
			'type' => array(
				'label' => LABEL_USER_TYPE,
				'catalog' => $USER_TYPE,
				'required' => true,
				'listable' => true
			),
			'name' => array(
				'label' => LABEL_USER_FULL_NAME,
				'required' => true,
				'listable' => true
			),
			'email' => array(
				'label' => LABEL_USER_EMAIL,
				'required' => true,
				'format' => 'email'
			),
			'status' => array(
				'label' => LABEL_USER_STATUS,
				'catalog' => $USER_STATUS,
				'default' => USER_STATUS_ACTIVE,
				'required' => true,
				'listable' => true
			)
		));
	}
}