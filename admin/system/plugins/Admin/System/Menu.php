<?php
namespace Admin\System;

class Menu {
	static function removeItem(&$menu, $itemValue, $property = 'path') {
		foreach($menu as $key => $item){
			if($item[$property] == $itemValue){
				unset($menu[$key]);
				return $item;
			}
		}
	}

	static function insertBefore(&$menu, $newItems, $beforeItemValue, $property = 'path'){
		$newMenu = [];

		if(isset($newItems['label'])){ // Support newItems being a single menu item
			$newItems = [$newItems];
		}

		foreach($menu as $item){
			if($item[$property] == $beforeItemValue && $newItems){
				$newMenu = array_merge($newMenu, $newItems);
				$newItems = null;
			}

			$newMenu[] = $item;
		}

		$menu = $newMenu;
	}

	static function addItems(&$menu, $items){
		\Util::extend($menu, $items);
	}
}