<?php
global $LOGIN, $ADMIN_MENU;

APP::onPluginsLoad(function(){
	global $LOGIN, $USER, $ADMIN_MENU;

	$LOGIN = new Login('admin/login', 'admin/logout');
	
	$LOGIN->setDefaultRoute('admin/home')
	->addUserType(USER_TYPE_ADMIN, array())
	->addUserType(USER_TYPE_SUPERVISOR, array('!admin/system/'))
	->setAccessTree($ADMIN_MENU)
	->disableOn('admin/recover-password');
});