<?php
class CoreAdminLogin_MasterController extends Controller {
	function main($APP){
		global $LOGIN, $USER;

		// Validate the login
		$LOGIN->load();

		if($LOGIN->authorized()){
			$USER = $LOGIN->user->data;
		}
		else{
			$USER = false;
		}
	}
}