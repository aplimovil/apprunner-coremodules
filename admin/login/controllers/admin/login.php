<?php
class CoreAdminLogin_AdminLoginController extends Controller {
	protected $invalidLogin;

	public function main($APP){
		$this->validate();
		$this->render();
	}

	function validate($allowedUserTypes = null){
		global $LOGIN;

		$this->invalidLogin = false;

		if(ltrim($_POST['username']) != ""){
			$user = user::findOne(["username" => $_POST['username'], "status" => USER_STATUS_ACTIVE]);

			if($user){
				if(!$allowedUserTypes || in_array($user->type, $allowedUserTypes)){
					if($user->validate($_POST['password'])){
						user_activity::record([
							"user_id" => $user->id,
							"type" => USER_ACTIVITY_TYPE_LOGIN
						]);
						
						$LOGIN->start($user->username, $user->type, $user->getData(), $_POST['p']);
					}
					else $this->invalidLogin = true;
				}
			}
			else $this->invalidLogin = true;
		}
	}

	function render(){
		// The template is located in the core/lang modules
		$this->loadTemplate('admin/login', [
			'invalidLogin' => $this->invalidLogin
		]);

		$this->disableView();
	}
}