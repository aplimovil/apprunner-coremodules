<?php
class CoreAdminLogin_AdminLogoutController extends Controller {
	public function main($APP){
		global $LOGIN;
		
		$user = $LOGIN->getUserData();

		user_activity::record(array(
			"user_id" => $user->id,
			"type" => USER_ACTIVITY_TYPE_LOGOUT
		));
					
		$LOGIN->end();
	}
}