<?php
if(!function_exists('__CoreAdminMenu_getAttributesString')){
    function __CoreAdminMenu_getAttributesString($aAttributes){
        $aAttributesString = array();

        if($aAttributes){
            foreach($aAttributes as $sName => $sValue){
                $aAttributesString[] = "{$sName}=\"{$sValue}\"";
            }
        }

        $sAttributes = count($aAttributesString) != 0 ? (' ' . join(' ', $aAttributesString)) : '';

        return $sAttributes;
    }
}
?>
<ul id="navigation" class="sf-navbar">
    <?php foreach($LOGIN->getUserAccessTree() as $item): if(!$item || $item->condition === false || $item->hidden) continue; ?>
    <li<?= __CoreAdminMenu_getAttributesString($item->attributes) ?>>
        <a href="<?= $item->inactive ? '#' : $LOGIN->translatePath($item->path) ?>"<?php if($item->inactive) echo ' onclick="return false;"'; ?><?= __CoreAdminMenu_getAttributesString($item->linkAttributes) ?>><?= $item->label ?></a>
        <?php if($item->items): ?>
        <ul<?php if($item->width) echo " style=\"width: {$item->width}px;\""; ?>>
        <?php foreach($item->items as $subitem): if(!$subitem || $subitem->condition === false || $subitem->hidden) continue; ?>
          <li<?= __CoreAdminMenu_getAttributesString($subitem->attributes) ?>>
            <a href="<?= $LOGIN->translatePath($item->path, $subitem->path) ?>"<?= __CoreAdminMenu_getAttributesString($subitem->linkAttributes) ?>><?= $subitem->label ?></a>
          </li>
        <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>

<?php if($USER && $USER->username != 'admin'): ?>
<style type="text/css">
#navigation ul li.master-admin {
    display: none;
}
</style>
<?php endif; ?>