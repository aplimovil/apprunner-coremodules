<?php
class CoreAdminCore_MasterController extends Controller {
	static $startSession = true;
	
	function main($APP){
		$this->addCSS('admin/style');
		$this->addJS('admin/admin', 'admin/superfish', 'admin/tooltip', 'admin/cookie');
	}
}